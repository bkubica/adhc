
struct DerivFactory {
	template<typename T>
	static typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1, T>::type create (const int num_vars, Type2Type<T>) {
		// first and second drivative of a univariate function
		return T(0.0);
	};

	template<typename T>
	static typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1, T>::type
	create (const int num_vars, Type2Type<T>, const int) {
		// higher derivatives of a univariate function
		return T(0.0);
	};

	template<typename T>
	static typename std::enable_if<TL::IndexOf<vector_types, T>::value != -1, T>::type create (const int num_vars, Type2Type<T>) {
		// gradient of a multivariate function
		return T(num_vars);
	};

	template<typename T>
	static typename std::enable_if<TL::IndexOf<matrix_types, T>::value != -1, T>::type create (const int num_vars, Type2Type<T>) {
		// Hesse matrix of a multivariate function
		return T(num_vars, num_vars);
	};
};

