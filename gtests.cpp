#include <iostream>
#include <exception>
#include <stdexcept>
#include <string>
#include <sstream>

//#define BOOST_TEST_MODULE TestOfADHC
//#include <boost/test/unit_test.hpp>
#include <gtest/gtest.h>

#include <complex.hpp>
#include <civector.hpp>
#include <cimatrix.hpp>
#include <scivector.hpp>
#include <scimatrix.hpp>
#include <l_ivector.hpp>
#include <l_imatrix.hpp>

#include "adhc.hpp"


using namespace std;
using namespace adhc;
using namespace testing;


#define SPARSITY SparsityLevel::highly_sparse

/*struct ZlyMianownik : public exception {
	const char *what() const throw() {
		return "Zerowy mianownik ulamka!\n";
	};
};*/

template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f_add(const adhc_vector<lev, sparse_mode, n> &x) {
	return x[1] + x[2];
}

TEST (Addition, Equality) {
	cxsc::ivector x(2);
	const int num_vars = 2;
	x[1] = cxsc::interval(1.0, 2.0);
	x[2] = cxsc::interval(2.0, 4.0);
	adhc_vector<0, SPARSITY, num_vars> x_ (x);
	adhc_ari<0, SPARSITY, num_vars> y_ = f_add(x_);
	cxsc::interval y = std::get<0>(y_.deriv_value);
	cxsc::interval y_ref {3.0, 6.0};
	EXPECT_EQ (y, y_ref);
	//EXPECT_DOUBLE_EQ (y, y_ref);
}


//****************************************************


template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f_sub(const adhc_vector<lev, sparse_mode, n> &x) {
	return x[1] - x[2];
}

TEST (Subtraction, Equality) {
	cxsc::ivector x(2);
	const int num_vars = 2;
	x[1] = cxsc::interval(1.0, 2.0);
	x[2] = cxsc::interval(2.0, 4.0);
	adhc_vector<1, SPARSITY, num_vars> x_ (x);
	adhc_ari<1, SPARSITY, num_vars> y_ = f_sub(x_);
	//std::cout << y_;

	cxsc::interval y = std::get<0>(y_.deriv_value);
	cxsc::ivector grad_y = std::get<1>(y_.deriv_value);

	cxsc::interval y_ref {-3.0, 0.0};
	cxsc::ivector grad_y_ref(2);
	grad_y_ref[1] = cxsc::interval(1.0);
	grad_y_ref[2] = cxsc::interval(-1.0);

	EXPECT_EQ (y, y_ref);
	EXPECT_EQ (grad_y, grad_y_ref);
}


//****************************************************


template<int lev, SparsityLevel sparse_mode, int n, typename T = cxsc::interval>
adhc_ari<lev, sparse_mode, n, T> f_comp_add(const adhc_vector<lev, sparse_mode, n, T> &x) {
	auto result = adhc_ari<lev, sparse_mode, n, T>::get_instance();
	result = T(1.0);
	for (int i = 1; i <= n; ++i) result += x[i];
	return result - sqr(x[1]);
}

//BOOST_PARAM_TEST_CASE (test_composite_addition, level)
TEST (CompositeAddition, Equality) {
	const int num_vars = 4;
	cxsc::ivector x(num_vars);
	for (int i = 1; i <= num_vars; ++i) x[i] = cxsc::interval(1.0, 2.0);
	adhc_vector<1, SPARSITY, num_vars> x_ (x);
	adhc_ari<1, SPARSITY, num_vars> y_ = f_comp_add(x_);
	//std::cout << y_;

	cxsc::interval y = std::get<0>(y_.deriv_value);
	cxsc::ivector grad_y = std::get<1>(y_.deriv_value);

	cxsc::interval y_ref {1.0, 8.0};
	cxsc::ivector grad_y_ref(num_vars);
	grad_y_ref = cxsc::interval(1.0);
	grad_y_ref[1] = cxsc::interval(1.0 - 2*x[1]);

	EXPECT_EQ (y, y_ref);
	EXPECT_EQ (grad_y, grad_y_ref);

	adhc_vector<-1, SPARSITY, num_vars> xx_ (x);
	//adhc_ari<-1, SPARSITY, num_vars> *yy_ptr = new adhc_ari<-1, SPARSITY, num_vars>;
	//adhc_ari<-1, SPARSITY, num_vars> &yy_ = *yy_ptr;
	adhc_ari<-1, SPARSITY, num_vars> yy_;
	yy_ = f_comp_add(xx_);
	std::cout << yy_ << "\n";
}


//****************************************************


struct LevelParams : ::testing::TestWithParam<int> {
};

TEST_P (LevelParams, Test1) {
	std::cout << "\n";
	//EXPECT_FALSE(true);
}

INSTANTIATE_TEST_CASE_P (Level, LevelParams, Values(0, 1, 2, -1));


//****************************************************


struct SparsityParams : ::testing::TestWithParam<SparsityLevel> {
	//const SpLevel = SparsityLevel;
};

TEST_P (SparsityParams, Test1) {
	const int n = 3;
	const SparsityLevel sparse_mode = GetParam();
	std::cout << "Level: " << static_cast<int>(GetParam()) << "\n";
	using ad_type = adhc_ari<2, SparsityLevel::highly_sparse/*sparse_mode*/, n>;
	;
}

INSTANTIATE_TEST_CASE_P (Sparsity, SparsityParams, Values(SparsityLevel::dense, SparsityLevel::sparse, SparsityLevel::highly_sparse));


//****************************************************


template <typename T>
struct BasicTypeTest : ::testing::Test {
	using MyType = T;
};

using BasicTypes = ::testing::Types<cxsc::interval, cxsc::cinterval, cxsc::l_interval, cxsc::real, cxsc::complex, cxsc::l_real>;

TYPED_TEST_CASE (BasicTypeTest, BasicTypes);

TYPED_TEST (BasicTypeTest, Test1) {
	using T  = typename TestFixture::MyType;
	std::cout << sizeof(T) << "\n";
	EXPECT_TRUE((std::is_same<T, typename adhc::Types<T>::scalar_type>::value));
	const int n = 3;
	adhc_ari<2, SparsityLevel::highly_sparse, n, T> xx_;
	xx_ = T(7.0);
	std::cout << xx_ << "\n";
}


//****************************************************


TEST (TestCompositeAddition, TestReal) {
	const int num_vars = 4;
	cxsc::rvector x(num_vars);
	for (int i = 1; i <= num_vars; ++i) x[i] = cxsc::real(i);
	adhc_vector<1, SPARSITY, num_vars, cxsc::real> x_ (x);
	adhc_ari<1, SPARSITY, num_vars, cxsc::real> y_ = f_comp_add<1, SPARSITY, num_vars, cxsc::real>(x_);
	//std::cout << y_;

	cxsc::real y = std::get<0>(y_.deriv_value);
	cxsc::rvector grad_y = std::get<1>(y_.deriv_value);

	cxsc::real y_ref {10.0};
	cxsc::rvector grad_y_ref(num_vars);
	grad_y_ref = cxsc::real(1.0);
	grad_y_ref[1] = cxsc::real(1.0 - 2*x[1]);

	EXPECT_EQ (y, y_ref);
	EXPECT_EQ (grad_y, grad_y_ref);

	adhc_vector<-1, SPARSITY, num_vars> xx_ {cxsc::ivector(x)};
	//adhc_ari<-1, SPARSITY, num_vars> *yy_ptr = new adhc_ari<-1, SPARSITY, num_vars>;
	//adhc_ari<-1, SPARSITY, num_vars> &yy_ = *yy_ptr;
	adhc_ari<-1, SPARSITY, num_vars> yy_;
	yy_ = f_comp_add(xx_);
	std::cout << yy_ << "\n";
}


//****************************************************


TEST (UnderlyingPointTypeTest, Test1) {
	using some_type = underlying_point_type<cxsc::interval>::type;
	std::cout << typeid(some_type).name() << std::endl;
	some_type x;
	x = 44.77;
	std::cout << "x = " << x << "\n";
	bool val = std::is_same<some_type, cxsc::real>::value;
	EXPECT_TRUE(val);

	bool val2 = std::is_same<underlying_point_type<cxsc::l_interval>::type, cxsc::l_real>::value;
	EXPECT_TRUE(val2);

	underlying_point_type<cxsc::ivector>::type xx(2);
	xx[1] = 1; xx[2] = 2;
	std::cout << "xx = " << xx << "\n";
	bool val3 = std::is_same<underlying_point_type<cxsc::ivector>::type, cxsc::rvector>::value;
	EXPECT_TRUE(val3);
}


//****************************************************


template<int lev, SparsityLevel sparse_mode, int n, class T = cxsc::interval>
adhc_ari<lev, sparse_mode, n, T> f_rational(const adhc_vector<lev, sparse_mode, n, T> &x) {
	adhc_ari<lev, sparse_mode, n, T> result;
	result = power(x[1], 4) + x[1]/x[2] + sqr(x[1]) + x[1]*x[2];
	return result;
}

/*template <typename T>
struct BasicTypeTest : ::testing::Test {
	using MyType = T;
};

using BasicTypes = ::testing::Types<cxsc::interval, cxsc::cinterval, cxsc::l_interval, cxsc::real, cxsc::complex, cxsc::l_real>;

TYPED_TEST_CASE (BasicTypeTest, BasicTypes);*/

TYPED_TEST (BasicTypeTest, TestDivisionForTypes) {
	std::cout << "TestDivisionForTypes: f_rational\n";
	using T  = typename TestFixture::MyType;
	std::cout << sizeof(T) << "\n";
	EXPECT_TRUE((std::is_same<T, typename adhc::Types<T>::scalar_type>::value));
	const int n = 2;
	typename adhc::Types<T>::dense_vector_type x(2);
	if (TL::IndexOf<interval_types, T>::value == -1) {
		std::cout << "T is a non-interval type\n";
		x[1] = 1.0;
		x[2] = 2.0;
		T y_ref{4.5};
	}
	else {
		// T is an interval type
		x[1] = 1.0;//T(1.0, 2.0);
		x[2] = 2.0;//T(-2.0, 2.0);
		T y_ref{4.5};//();
	}
	adhc_vector<2, SparsityLevel::highly_sparse, n, T> xx_{x};
	std::cout << xx_[1] << "\n";
	std::cout << xx_[2] << "\n";
	adhc_ari<2, SparsityLevel::highly_sparse, n, T> yy_;
	yy_ = f_rational(xx_);
	std::cout << yy_ << "\n";
}


//****************************************************
/*BOOST_AUTO_TEST_CASE (test_nierownosci_ulamkow)
{
	Ulamek a{1, 2};
	Ulamek b{2, 3};
	BOOST_CHECK (a == b);
}

}

//INSTANTIATE_TYPED_TEST_CASE_P (BTTInstantiation, BasicTypeTest, BasicTypes);

BOOST_AUTO_TEST_CASE (test_nierownosci_ulamkow)
{
	Ulamek a{1, 2};
	Ulamek b{2, 3};
	BOOST_CHECK (a == b);
}

BOOST_AUTO_TEST_CASE (test_dodawania_ulamkow)
{
	Ulamek a{1, 2};
	Ulamek b{1, 3};
	Ulamek c{5, 6};
	BOOST_CHECK_EQUAL ((a + b), c);
}*/

