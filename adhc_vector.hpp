#if !defined __ADHC_VECTOR_HPP__
#define __ADHC_VECTOR_HPP__

#include "adhc_ari.hpp"


namespace adhc {


template<int level, SparsityLevel sparse_mode, int num_vars, typename T = cxsc::interval>
struct adhc_vector {
	adhc_ari<level, sparse_mode, num_vars, T> variables[num_vars];

	adhc_vector<level, sparse_mode, num_vars, T>() = default;

	adhc_vector<level, sparse_mode, num_vars, T> (const typename Types<T>::dense_vector_type &x) {
		for (int i = 1; i <= num_vars; ++i) {
			(*this)[i] = adhc_ari<level, sparse_mode, num_vars, T>::create_variable (i, x[i]);
		}
	}

	adhc_vector<level, sparse_mode, num_vars, T> (const typename Types<T>::dense_vector_type &x,
						      const cxsc::intvector &which_are_variables) {
		for (int i = 1; i <= num_vars; ++i) {
			if (which_are_variables[i]) {
				(*this)[i] = adhc_ari<level, sparse_mode, num_vars, T>::create_variable (i, x[i]);
			}
			else {
				(*this)[i] = adhc_ari<level, sparse_mode, num_vars, T> (x[i]);
			}
		}
	}

	adhc_vector<level, sparse_mode, num_vars, T> (const typename Types<T>::dense_vector_type &x, const int var_num) {
		for (int i = 1; i < var_num; ++i) (*this)[i] = adhc_ari<level, sparse_mode, num_vars, T> (x[i]);
		(*this)[var_num] = adhc_ari<level, sparse_mode, num_vars, T>::create_variable (var_num, x[var_num]);
		for (int i = var_num + 1; i <= num_vars; ++i) (*this)[i] = adhc_ari<level, sparse_mode, num_vars, T> (x[i]);
	}

	adhc_ari<level, sparse_mode, num_vars, T> & operator [] (const int n) {
		return variables[n - 1]; //cxsc; for other interval libraries, it might be: ``return variables[n];''
	}

	const adhc_ari<level, sparse_mode, num_vars, T> & operator [] (const int n) const {
		return variables[n - 1]; //cxsc; for other interval libraries, it might be: ``return variables[n];''
	}
};

template<int level, SparsityLevel sparse_mode, int num_vars, typename T = cxsc::interval>
struct adhc_fun {
	//typedef adhc_ari<level, sparse_mode, num_vars> (* adhc_fun_ptr) (const adhc_vector<level, sparse_mode, num_vars> &);
	using adhc_fun_ptr = adhc_ari<level, sparse_mode, num_vars, T> (*) (const adhc_vector<level, sparse_mode, num_vars, T> &);
};


}


#endif

