#if !defined __ADHC_ARI_HPP__
#define __ADHC_ARI_HPP__

//#include <tuple>

#include <assert.h>

#include <ivector.hpp>
#include <imatrix.hpp>
#include <sivector.hpp>
#include <simatrix.hpp>

namespace cxsc {
	class civector;
	class cimatrix;
	class scivector;
	class scimatrix;
	class l_ivector;
	class l_imatrix;
}

//#include <civector.hpp>
//#include <cimatrix.hpp>
//#include <scivector.hpp>
//#include <scimatrix.hpp>
//#include <l_ivector.hpp>
//#include <l_imatrix.hpp>
#include <xscclass.hpp>

#include <xi_ari.hpp>

//#include <loki/TypeManip.h>
//#include <loki/Typelist.h>
//#include <loki/HierarchyGenerators.h>

#include "typemanip.hpp"
#include "typelist.hpp"
#include "max.hpp"

class itensor;
class sitensor;
class citensor;
class scitensor;
class l_itensor;
class rtensor;
class srtensor;
class ctensor;
class sctensor;
class l_ctensor;


namespace adhc {


#include "adhc_defs.hpp"
#include "type_traits.hpp"
#include "adhc_node.hpp"

enum class SparsityLevel {dense, sparse, highly_sparse, another_sparse};

template<int cur_level, int level, SparsityLevel sparse_mode, int num_var, typename T = cxsc::interval> class adhc_operations;
template<int cur_level, int level, SparsityLevel sparse_mode, int num_var, typename T = cxsc::interval> class adhc_operations_1;

template<int level, SparsityLevel sparse_mode, int num_vars, typename T = cxsc::interval> struct adhc_ari;

#include "tensor_product.hpp"

#include "adhc_template_interval_funs.hpp"
#include "adhc_division.hpp"

//********************************************************

template<SparsityLevel sparse_mode, int num_vars, typename T = cxsc::interval>
struct adhc_types {
	using fun_val_type = typename Types<T>::scalar_type;
	using grad_type = typename Types<T>::dense_vector_type;
	using Hesse_type = typename Types<T>::dense_matrix_type;
	using higher_deriv_type = typename Types<T>::dense_tensor_type;
};

template<int num_vars, typename T> struct adhc_types<SparsityLevel::sparse, num_vars, T> {
	using fun_val_type = typename Types<T>::scalar_type;
	using grad_type = typename Types<T>::dense_vector_type;
	using Hesse_type = typename Types<T>::sparse_matrix_type;
	using higher_deriv_type = typename Types<T>::sparse_tensor_type;
};

template<int num_vars, typename T> struct adhc_types<SparsityLevel::highly_sparse, num_vars, T> {
	using fun_val_type = typename Types<T>::scalar_type;
	using grad_type = typename Types<T>::sparse_vector_type;
	using Hesse_type = typename Types<T>::sparse_matrix_type;
	using higher_deriv_type = typename Types<T>::sparse_tensor_type;
};

template<int num_vars, typename T> struct adhc_types<SparsityLevel::another_sparse, num_vars, T> {
	using fun_val_type = typename Types<T>::scalar_type;
	using grad_type = typename Types<T>::sparse_vector_type;
	using Hesse_type = typename Types<T>::dense_matrix_type;
	using higher_deriv_type = typename Types<T>::dense_tensor_type;
};

template<typename T> struct adhc_types<SparsityLevel::dense, 1, T> {
	using fun_val_type = T;
	using grad_type = T;
	using Hesse_type = T;
	using higher_deriv_type = T;
};

template<typename T> struct adhc_types<SparsityLevel::sparse, 1, T> {
	using fun_val_type = T;
	using grad_type = T;
	using Hesse_type = T;
	using higher_deriv_type = T;
};

template<typename T> struct adhc_types<SparsityLevel::highly_sparse, 1, T> {
	using fun_val_type = T;
	using grad_type = T;
	using Hesse_type = T;
	using higher_deriv_type = T;
};

template<typename T> struct adhc_types<SparsityLevel::another_sparse, 1, T> {
	using fun_val_type = T;
	using grad_type = T;
	using Hesse_type = T;
	using higher_deriv_type = T;
};

//*************************************************************

template<int level, SparsityLevel sparse_mode, int num_vars, typename T = cxsc::interval> struct deriv_types;

template<SparsityLevel sparse_mode, int num_vars, typename T>
struct deriv_types<0, sparse_mode, num_vars, T> {
	using fun_val_type = typename adhc_types<sparse_mode, num_vars, T>::fun_val_type;
	using deriv_types_list = TL::TypeList<fun_val_type>;
};

template<SparsityLevel sparse_mode, int num_vars, typename T>
struct deriv_types<1, sparse_mode, num_vars, T> {
	using tlist_1 = typename deriv_types<0, sparse_mode, num_vars, T>::deriv_types_list;
	using grad_type = typename adhc_types<sparse_mode, num_vars, T>::grad_type;
	using deriv_types_list = typename TL::Append< tlist_1, grad_type>::Result;
};

template<SparsityLevel sparse_mode, int num_vars, typename T>
struct deriv_types<2, sparse_mode, num_vars, T> {
	using tlist_1 = typename deriv_types<1, sparse_mode, num_vars, T>::deriv_types_list;
	using Hesse_type = typename adhc_types<sparse_mode, num_vars, T>::Hesse_type;
	using deriv_types_list = typename TL::Append< tlist_1, Hesse_type>::Result;
};

template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
struct deriv_types {
	using tlist_1 = typename deriv_types<level - 1, sparse_mode, num_vars, T>::deriv_types_list;
	using higher_deriv_type = typename adhc_types<sparse_mode, num_vars, T>::higher_deriv_type;
	using deriv_types_list = typename TL::Append< tlist_1, higher_deriv_type>::Result;
};

//*************************************************************

template <class T>
typename std::enable_if<TL::IndexOf<vector_types, T>::value != -1, void>::type
assign_value_to_component(T &var, const int i, const cxsc::real &value) {
	var[i] = value;
}

template <class T>
typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1, void>::type
assign_value_to_component(T &var, const int i, const cxsc::real &value) {
	var = value;
}

//*************************************************************

#include "deriv_of_product.hpp"

template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
struct adhc_ari {
	using deriv_types_list = typename deriv_types<level, sparse_mode, num_vars, T>::deriv_types_list;

	typename TL::CreateTuple< deriv_types_list >::Result deriv_value;

	adhc_ari() {
		adhc_operations<level, level, sparse_mode, num_vars, T>::init(*this, T(0.0));
	}

	template <typename TT = T>
	adhc_ari (typename std::enable_if<!std::is_same<TT, cxsc::real>::value, const T>::type &value_) {
		//std::cout << "Constructor of a constant or variable\n";
		adhc_operations<level, level, sparse_mode, num_vars, T>::init(*this, value_);
	}

	adhc_ari (const cxsc::real &value_) {
		adhc_operations<level, level, sparse_mode, num_vars, T>::init(*this, T(value_));
	}

	std::string to_string() {
		return adhc_operations<level, level, sparse_mode, num_vars, T>::to_string(*this);
	}

	static adhc_ari<level, sparse_mode, num_vars, T> create_variable (const int i, const T &x) {
		assert(i <= num_vars);
		adhc_ari<level, sparse_mode, num_vars, T> u(x);
		adhc_operations_1<level, level, sparse_mode, num_vars, T>::set_gradient_component (u, i, 1.0);
		return u;
	}

	static adhc_ari<level, sparse_mode, num_vars, T> get_instance() {
		return adhc_ari<level, sparse_mode, num_vars, T>();
	}
};

template<SparsityLevel sparse_mode, int num_vars, typename T>
struct adhc_ari<-1, sparse_mode, num_vars, T> : adhc_node {

	adhc_ari() : adhc_node() {}

	template <typename TT,
		  typename = typename std::enable_if<TL::IndexOf<scalar_types, TT>::value != -1 || std::is_same<TT, double>::value || std::is_same<TT, adhc_node>::value, TT>::type>
	adhc_ari (const TT &value_) : adhc_node(value_) {}

	adhc_ari & operator = (const adhc_ari &u) {
		//std::cout << "adhc_ari = operator\n";
		//return static_cast<adhc_ari &>(operator=(static_cast<const adhc_node &>(u)));
		//return operator=(static_cast<const adhc_node &>(u));
		//std::cout << "adhc_ari operator = " << static_cast<int>(u.node_type) << "\n";
		//if (u.node_type == NodeType::variable) std::cout << "x[" << u.var_num << "] = " << u.value << "\n";
		if (this == &u) {
			std::cout << "WOW!!!\n\n\n";
			return *this;
		}
		if (this->node_type != NodeType::undefined) {
			std::cout << "Assigning to a defined adhc_node!!!\n\n\n\n";
		}
		node_type = u.node_type;
		value = u.value;
		switch(node_type) {
			case NodeType::variable: var_num = u.var_num;
						 break;
			case NodeType::function:
					function_params.arg = new adhc_node (* u.function_params.arg);
					function_params.fun = u.function_params.fun;
					break;
			case NodeType::int_power:
					int_power_params.arg = new adhc_node (* u.int_power_params.arg);
					int_power_params.exponent = u.int_power_params.exponent;
					break;
			case NodeType::operation:
					operation_params.left = new adhc_node (* u.operation_params.left);
					operation_params.right = new adhc_node (* u.operation_params.right);
					operation_params.oper = u.operation_params.oper;
					break;
			case NodeType::constant: return *this;
			case NodeType::undefined: return *this;
			//default: return *this; // this covers undefined and constant
		}
		return *this;
	}

	static adhc_ari<-1, sparse_mode, num_vars, T> create_variable (const int i, const cxsc::interval &x) {
		assert(i <= num_vars);
		adhc_ari<-1, sparse_mode, num_vars, T> u(x);
		u.node_type = NodeType::variable;
		u.var_num = i;
		return u;
	}

	static adhc_ari<-1, sparse_mode, num_vars, T> create_variable (const int i, const cxsc::real &x) {
		assert(i <= num_vars);
		adhc_ari<-1, sparse_mode, num_vars, T> u(x);
		u.node_type = NodeType::variable;
		u.var_num = i;
		return u;
	}

	static adhc_ari<-1, sparse_mode, num_vars, T> & get_instance() {
		adhc_ari<-1, sparse_mode, num_vars, T> *ptr = new adhc_ari<-1, sparse_mode, num_vars, T>();
		return *ptr;
	}

	/*~adhc_ari() {
		//this->adhc_node::~adhc_node();
		//if (this != nullptr) this->~adhc_node();
	}*/
};


//******************************************************************


#include "factory.hpp"


template<int cur_level, int level, SparsityLevel sparse_mode, int num_vars, typename T>
struct adhc_operations_1 {
	// they have to be put in a class, so that they could have partial specializations
	// this struct contains theese that need only general implementation and specialization for cur_level == 0

	static inline void set_gradient_component (adhc_ari<level, sparse_mode, num_vars, T> &u, const int i, const cxsc::real &r) {
		assign_value_to_component (std::get<1>(u.deriv_value), i, r);
	}

	static inline void add (const adhc_ari<level, sparse_mode, num_vars, T> &u,
				const adhc_ari<level, sparse_mode, num_vars, T> &v,
				adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations_1<cur_level - 1, level, sparse_mode, num_vars, T>::add(u, v, result);
		std::get<cur_level>(result.deriv_value) = std::get<cur_level>(u.deriv_value) + std::get<cur_level>(v.deriv_value);
	}

	static inline void add (adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
		adhc_operations_1<cur_level - 1, level, sparse_mode, num_vars, T>::add(u, v);
		std::get<cur_level>(u.deriv_value) += std::get<cur_level>(v.deriv_value);
	}

	static inline void sub (const adhc_ari<level, sparse_mode, num_vars, T> &u,
				const adhc_ari<level, sparse_mode, num_vars, T> &v,
				adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations_1<cur_level - 1, level, sparse_mode, num_vars, T>::sub(u, v, result);
		std::get<cur_level>(result.deriv_value) = std::get<cur_level>(u.deriv_value) - std::get<cur_level>(v.deriv_value);
	}

	static inline void sub (adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
		adhc_operations_1<cur_level - 1, level, sparse_mode, num_vars, T>::sub(u, v);
		std::get<cur_level>(u.deriv_value) -= std::get<cur_level>(v.deriv_value);
	}

	static inline void negate (const adhc_ari<level, sparse_mode, num_vars, T> &u,
				   adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations_1<cur_level - 1, level, sparse_mode, num_vars, T>::negate(u, result);
		std::get<cur_level>(result.deriv_value) = -std::get<cur_level>(u.deriv_value);
	}

	template<class TT>
	static inline void multiply (adhc_ari<level, sparse_mode, num_vars, T> &u, const TT/*cxsc::real*/ &x) {
		adhc_operations_1<cur_level - 1, level, sparse_mode, num_vars, T>::multiply(u, x);
		std::get<cur_level>(u.deriv_value) *= x;
	}

	static inline void intersection (const adhc_ari<level, sparse_mode, num_vars, T> &u,
					 const adhc_ari<level, sparse_mode, num_vars, T> &v,
					 adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations_1<cur_level - 1, level, sparse_mode, num_vars, T>::intersection(u, v, result);
		std::get<cur_level>(result.deriv_value) = std::get<cur_level>(u.deriv_value) &
							     std::get<cur_level>(v.deriv_value);
	}

};


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
struct adhc_operations_1<0, level, sparse_mode, num_vars, T> {

	static inline void set_gradient_component (adhc_ari<level, sparse_mode, num_vars, T> &u, const int i, const cxsc::real &r) {
	}

	static inline void add(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			       const adhc_ari<level, sparse_mode, num_vars, T> &v,
			       adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = std::get<0>(u.deriv_value) + std::get<0>(v.deriv_value);
	}

	static inline void add(adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
		std::get<0>(u.deriv_value) += std::get<0>(v.deriv_value);
	}

	static inline void sub(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			       const adhc_ari<level, sparse_mode, num_vars, T> &v,
			       adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = std::get<0>(u.deriv_value) - std::get<0>(v.deriv_value);
	}

	static inline void sub(adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
		std::get<0>(u.deriv_value) -= std::get<0>(v.deriv_value);
	}

	static inline void negate (const adhc_ari<level, sparse_mode, num_vars, T> &u,
				   adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = -std::get<0>(u.deriv_value);
	}

	template<class TT>
	static inline void multiply (adhc_ari<level, sparse_mode, num_vars, T> &u, const TT/*cxsc::real*/ &x) {
		std::get<0>(u.deriv_value) *= x;
	}

	static inline void intersection(const adhc_ari<level, sparse_mode, num_vars, T> &u,
				        const adhc_ari<level, sparse_mode, num_vars, T> &v,
				        adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = std::get<0>(u.deriv_value) & std::get<0>(v.deriv_value);
	}
};

//******************************************************************

cxsc::ivector hull_with_zero (const cxsc::ivector &x) {
	cxsc::ivector result(x);
	for (int i = 1; i <= VecLen(x); ++i) result[i] |= 0.0;
	return result;
}

cxsc::sivector hull_with_zero (const cxsc::sivector &x) {
	cxsc::sivector result(x);
	const int n = result.values().size();
	for (int i = 0; i < n; ++i) result.values()[i] |= 0.0;
	return result;
}

template<class T>
typename std::enable_if<TL::IndexOf<vector_types, T>::value != -1, T>::type hull_with_zero (const T &x) {
	T result(x);
	for (int i = 1; i <= VecLen(x); ++i) result[i] |= 0.0;
	return result;
}


void compute_Hesse_matrix_of_max (const cxsc::ivector &grad_u, const cxsc::ivector &grad_v, 
				  const cxsc::imatrix &H_u, const cxsc::imatrix &H_v, cxsc::imatrix &H) {
	//std::cout << "Possibly implemented...\n";
	for (int i = 1; i <= VecLen(grad_u); ++i) {
		if (grad_u[i] == 0.0) continue;
		if (Inf(grad_u[i]) == Sup(grad_u[i]) && grad_u[i] == grad_v[i]) continue;
		//bool thin = (Inf(grad_u[i]) == Sup(grad_u[i]));
		for (int j = i; j <= VecLen(grad_u); ++j) {
			if (grad_u[j] == 0.0) continue;
			if (Inf(grad_u[j]) == Sup(grad_u[j]) && grad_u[j] == grad_v[j]) continue;
			H[i][j] = H[j][i] = H_u[i][j] | H_v[i][j] | cxsc::interval(0.0, cxsc::Infinity);
		}
	}
}

void compute_Hesse_matrix_of_max (const cxsc::ivector &grad_u, const cxsc::ivector &grad_v, 
				  const cxsc::simatrix &H_u, const cxsc::simatrix &H_v, cxsc::simatrix &H) {
	//std::cout << "Possibly implemented - partially sparse...\n";
	cxsc::imatrix H_aux(VecLen(grad_u), VecLen(grad_u));
	H_aux = 0.0;
	for (int i = 1; i <= VecLen(grad_u); ++i) {
		if (grad_u[i] == 0.0) continue;
		if (Inf(grad_u[i]) == Sup(grad_u[i]) && grad_u[i] == grad_v[i]) continue;
		//bool thin = (Inf(grad_u[i]) == Sup(grad_u[i]));
		for (int j = i; j <= VecLen(grad_u); ++j) {
			if (grad_u[j] == 0.0) continue;
			if (Inf(grad_u[j]) == Sup(grad_u[j]) && grad_u[j] == grad_v[j]) continue;
			H_aux[i][j] = H_aux[j][i] = H_u[i][j] | H_v[i][j] | cxsc::interval(0.0, cxsc::Infinity);
		}
	}
	H |= H_aux;
}

void compute_Hesse_matrix_of_max (const cxsc::sivector &grad_u, const cxsc::sivector &grad_v, 
				  const cxsc::simatrix &H_u, const cxsc::simatrix &H_v, cxsc::simatrix &H) {
	/*std::cout << "Possibly implemented - sparse version...\n";
	std::cout << "compute Hesse\n";
	std::cout << "grad_u.values() = ";
	for (int ii = 0; ii < grad_u.values().size(); ++ii) std::cout << grad_u.values()[ii] << ", ";
	std::cout << "\n";
	std::cout << "grad_u.row_indices() = ";
	for (int ii = 0; ii < grad_u.row_indices().size(); ++ii) std::cout << grad_u.row_indices()[ii] << ", ";
	std::cout << "\n";
	std::cout << "grad_v.values() = ";
	for (int ii = 0; ii < grad_v.values().size(); ++ii) std::cout << grad_v.values()[ii] << ", ";
	std::cout << "\n";
	std::cout << "grad_v.row_indices() = ";
	for (int ii = 0; ii < grad_v.row_indices().size(); ++ii) std::cout << grad_v.row_indices()[ii] << ", ";
	std::cout << "\n";*/
	sivector grad = grad_u + grad_v;
	const int n = grad.values().size();
	cxsc::imatrix H_aux(VecLen(grad_u), VecLen(grad_u));
	H_aux = 0.0;
	for (int ind = 0; ind < n; ++ind) {
		//std::cout << "ind = " << ind << "\n";
		if (grad.values()[ind] == 0.0) continue;
		//if (Inf(grad.values()[ind]) == Sup(grad.values()[ind]) && grad_u.values()[ind] == grad_v.values()[ind]) continue;
		const int i = grad.row_indices()[ind] + 1; //cxsc
		for (int ind2 = 0; ind2 < n; ++ind2) {
			//std::cout << "ind2 = " << ind2 << "\n";
			if (grad.values()[ind2] == 0.0) continue;
			//if (Inf(grad_u.values()[ind2]) == Sup(grad_u.values()[ind2]) && grad_u.values()[ind2] == grad_v.values()[ind2]) continue;
			const int j = grad.row_indices()[ind2] + 1; //cxsc
			//std::cout << "i = " << i << ", j = " << j << "\n";
			H_aux[i][j] = H_aux[j][i] = H_u[i][j] | H_v[i][j] | cxsc::interval(0.0, cxsc::Infinity);
		}
	}
	H |= H_aux;
}

void compute_Hesse_matrix_of_max_with_const (const cxsc::ivector &grad_u, const cxsc::imatrix &H_u, cxsc::imatrix &H) {
	// at this point we assume Inf(u) < 0.0 < Sup(u)
	const int n = VecLen(grad_u);
	for (int i = 1; i <= n; ++i) {
		if (grad_u[i] != 0.0) {
			for (int j = i; j <= n; ++j)
				if (grad_u[j] != 0.0) H[i][j] = H[j][i] = H_u[i][j] | cxsc::interval(0.0, cxsc::Infinity);
		}
	}
}

void compute_Hesse_matrix_of_max_with_const (const cxsc::ivector &grad_u, const cxsc::simatrix &H_u, cxsc::simatrix &H) {
	// at this point we assume Inf(u) < 0.0 < Sup(u)
	const int n = VecLen(grad_u);
	cxsc::imatrix H_aux(VecLen(grad_u), VecLen(grad_u));
	H_aux = 0.0;
	for (int i = 1; i <= n; ++i) {
		if (grad_u[i] != 0.0) {
			for (int j = i; j <= n; ++j)
				if (grad_u[j] != 0.0) H_aux[i][j] = H_aux[j][i] = H_u[i][j] | cxsc::interval(0.0, cxsc::Infinity);
		}
	}
	H = H_aux;
}

void compute_Hesse_matrix_of_max_with_const (const cxsc::sivector &grad_u, const cxsc::simatrix &H_u, cxsc::simatrix &H) {
	/*std::cout << "compute Hesse\n";
	std::cout << "grad_u.values() = ";
	for (int ii = 0; ii < grad_u.values().size(); ++ii) std::cout << grad_u.values()[ii] << ", ";
	std::cout << "\n";
	std::cout << "grad_u.row_indices() = ";
	for (int ii = 0; ii < grad_u.row_indices().size(); ++ii) std::cout << grad_u.row_indices()[ii] << ", ";
	std::cout << "\n";*/
	// at this point we assume Inf(u) < 0.0 < Sup(u)
	const int n = grad_u.values().size();//VecLen(grad_u);
	cxsc::imatrix H_aux(VecLen(grad_u), VecLen(grad_u));
	H_aux = 0.0;
	for (int ind = 0; ind < n; ++ind) {
		if (grad_u.values()[ind] != 0.0) { // should we really check this condition???
			const int i = grad_u.row_indices()[ind] + 1; //cxsc
			for (int ind2 = 0; ind2 < n; ++ind2) {
				if (grad_u.values()[ind2] != 0.0) {
					const int j = grad_u.row_indices()[ind2] + 1; //cxsc
					//std::cout << "i = " << i << ", j = " << j << "\n";
					H_aux[i][j] = H_u[i][j] | cxsc::interval(0.0, cxsc::Infinity);
				}
			}
		}
	}
	H = H_aux;
}


//******************************************************************


template<int cur_level, int level, SparsityLevel sparse_mode, int num_vars, typename T>
class adhc_operations {
	// they have to be put in a class, so that they could have partial specializations

  public:
	static void init(adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
		adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::init(u, x);
		using deriv_types_list = typename adhc_ari<level, sparse_mode, num_vars, T>::deriv_types_list;
		using cur_deriv_type = typename TL::TypeAt<deriv_types_list, cur_level>::Result;
		//std::get<cur_level>(u.deriv_value) = cur_deriv_type(num_vars);
		std::get<cur_level>(u.deriv_value) = DerivFactory::create(num_vars, Type2Type<cur_deriv_type>(), cur_level);
		std::get<cur_level>(u.deriv_value) = 0.0;
		//std::cout << "initializing: " << cur_level << "\n";
	}

	static void output(std::ostream &s, const adhc_ari<level, sparse_mode, num_vars, T> &u) {
		adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::output(s, u);
		s << "deriv(" << cur_level << ") = " << std::get<cur_level>(u.deriv_value) << "\n";
	}

	static std::string to_string(const adhc_ari<level, sparse_mode, num_vars, T> &u) {
		return adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::to_string(u) + "deriv(" + std::to_string(cur_level) + ") = " + std::to_string(std::get<cur_level>(u.deriv_value)) + "\n";
	}

	static void multiply (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			      const adhc_ari<level, sparse_mode, num_vars, T> &v,
			      adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::multiply(u, v, result);
		std::get<cur_level>(result.deriv_value) = derivative_of_product<level, sparse_mode, num_vars, T, cur_level>(u, v);
	}

	static void sqr(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::sqr(u, result);
		//std::get<cur_level>(result.deriv_value) = FunOperType::multiplication_auxiliary_operations<level, sparse_mode, num_vars, 2, 1>::summand(u, u);
		//std::get<cur_level>(result.deriv_value) = tensor_product(tensor_product(tensor_product(std::get<0>(u.deriv_value), std::get<cur_level>(u.deriv_value)), cxsc::interval(binom_coef<cur_level, 0>::value)) + tensor_product(tensor_product(std::get<1>(u.deriv_value), std::get<1>(u.deriv_value)), cxsc::interval(binom_coef<cur_level, cur_level>::value)), cxsc::interval(2.0));
		std::get<cur_level>(result.deriv_value) = 2.0*(tensor_product(tensor_product(std::get<0>(u.deriv_value), std::get<cur_level>(u.deriv_value)), cxsc::interval(binom_coef<cur_level, 0>::value)) + tensor_product(tensor_product(std::get<1>(u.deriv_value), std::get<1>(u.deriv_value)), cxsc::interval(binom_coef<cur_level, cur_level>::value)));
		// ????????????????
	}

	static void power(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			  const int n,
			  adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::power(u, n, result);
		std::get<cur_level>(result.deriv_value) = 0.0; //????????
	}

	static void exp(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::exp(u, result);
		std::get<cur_level>(result.deriv_value) = 0.0; //????????
	}

	static void sin(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::sin(u, result);
		std::get<cur_level>(result.deriv_value) = 0.0; //????????
	}

	static void cos(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<cur_level - 1, level, sparse_mode, num_vars, T>::cos(u, result);
		std::get<cur_level>(result.deriv_value) = 0.0; //????????
	}
};



template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
class adhc_operations<2, level, sparse_mode, num_vars, T> {

  public:
	static void init(adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::init(u, x);
		using deriv_types_list = typename adhc_ari<level, sparse_mode, num_vars, T>::deriv_types_list;
		using cur_deriv_type = typename TL::TypeAt<deriv_types_list, 2>::Result;
		std::get<2>(u.deriv_value) = DerivFactory::create(num_vars, Type2Type<cur_deriv_type>());
		std::get<2>(u.deriv_value) = 0.0;
	}

	static void output(std::ostream &s, const adhc_ari<level, sparse_mode, num_vars, T> &u) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::output(s, u);
		s << "Hesse = deriv(" << 2 << ") = " << std::get<2>(u.deriv_value) << "\n";
	}

	static std::string to_string(const adhc_ari<level, sparse_mode, num_vars, T> &u) {
		return adhc_operations<1, level, sparse_mode, num_vars, T>::to_string(u) + "Hesse = deriv(2) = " + std::to_string(std::get<2>(u.deriv_value)) + "\n";
	}

	static void multiply (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			      const adhc_ari<level, sparse_mode, num_vars, T> &v,
			      adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::multiply(u, v, result);
		using deriv_types_list = typename adhc_ari<level, sparse_mode, num_vars, T>::deriv_types_list;
		typename TL::TypeAt<deriv_types_list, 2>::Result product_of_first_derivs = 
			tensor_product(std::get<1>(u.deriv_value), std::get<1>(v.deriv_value));
		std::get<2>(result.deriv_value) = 
			tensor_product(std::get<0>(u.deriv_value), std::get<2>(v.deriv_value)) + 
			product_of_first_derivs + 
			transp(product_of_first_derivs) + 
			tensor_product(std::get<2>(u.deriv_value), std::get<0>(v.deriv_value));
	}

	/*----static void multiply (adhc_ari<level, sparse_mode, num_vars> &u, const adhc_ari<level, sparse_mode, num_vars> &v) {
		adhc_operations<1, level, sparse_mode, num_vars>::multiply(u, v);
		using deriv_types_list = typename adhc_ari<level, sparse_mode, num_vars>::deriv_types_list;
		typename TL::TypeAt<deriv_types_list, 2>::Result product_of_first_derivs = 
			tensor_product(std::get<1>(u.deriv_value), std::get<1>(v.deriv_value));
		std::get<2>(u.deriv_value) = 
			tensor_product(std::get<0>(u.deriv_value), std::get<2>(v.deriv_value)) + 
			product_of_first_derivs + 
			transp(product_of_first_derivs) + 
			tensor_product(std::get<2>(u.deriv_value), std::get<0>(v.deriv_value));
	}*/

	static void divide (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			    const adhc_ari<level, sparse_mode, num_vars, T> &v,
			    adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::divide(u, v, result);
		typename adhc_types<sparse_mode, num_vars, T>::Hesse_type product_of_first_derivs =
			//tensor_product(std::get<1>(result.deriv_value), std::get<1>(v.deriv_value));
			tensor_product(std::get<1>(v.deriv_value), std::get<1>(result.deriv_value));
			// the order might be important: gradient of v does not contain NaNs or Infinities...
		typename adhc_types<sparse_mode, num_vars, T>::Hesse_type nominator = std::get<2>(u.deriv_value) -
			product_of_first_derivs -
			transp(product_of_first_derivs) -
			std::get<0>(result.deriv_value) * std::get<2>(v.deriv_value);
		/*std::cout << "first: " << std::get<1>(result.deriv_value) << "\n";
		std::cout << "second: " << std::get<1>(v.deriv_value) << "\n";
		std::cout << "prod: " << product_of_first_derivs << "\n";
		std::cout << "nominator: " << nominator << "\n";*/
		safe_division (nominator, std::get<0>(v.deriv_value), std::get<2>(result.deriv_value));
	}

	static void sqr(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::sqr(u, result);
		std::get<2>(result.deriv_value) = 2.0*(std::get<0>(u.deriv_value)*std::get<2>(u.deriv_value) + tensor_product_with_self(std::get<1>(u.deriv_value)));
	}

	static void power(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			  const int n,
			  adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::power(u, n, result);
		//std::get<2>(result.deriv_value) = n*(n - 1)*cxsc::power(std::get<0>(u.deriv_value), n - 2)*std::get<2>(u.deriv_value) + n*cxsc::power(std::get<0>(u.deriv_value), n - 1)*tensor_product_with_self(std::get<1>(u.deriv_value));
		//std::get<2>(result.deriv_value) = n*((n - 1)*cxsc::Power(std::get<0>(u.deriv_value), n - 2)*std::get<2>(u.deriv_value) + cxsc::Power(std::get<0>(u.deriv_value), n - 1)*tensor_product_with_self(std::get<1>(u.deriv_value)));
		std::get<2>(result.deriv_value) = n*((n - 1)*adhc::power(std::get<0>(u.deriv_value), n - 2)*std::get<2>(u.deriv_value) + adhc::power(std::get<0>(u.deriv_value), n - 1)*tensor_product_with_self(std::get<1>(u.deriv_value)));
	}

	static void exp(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::exp(u, result);
		//std::get<2>(result.deriv_value) = cxsc::exp(std::get<0>(u.deriv_value))*(std::get<2>(u.deriv_value) + tensor_product_with_self(std::get<1>(u.deriv_value)));
		std::get<2>(result.deriv_value) = std::get<0>(result.deriv_value)*(std::get<2>(u.deriv_value) + tensor_product_with_self(std::get<1>(u.deriv_value)));
	}

	static void sin(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::sin(u, result);
		//std::get<2>(result.deriv_value) = cxsc::cos(std::get<0>(u.deriv_value))*(std::get<2>(u.deriv_value)) - cxsc::sin(std::get<0>(u.deriv_value))*(tensor_product_with_self(std::get<1>(u.deriv_value)));
		std::get<2>(result.deriv_value) = cxsc::cos(std::get<0>(u.deriv_value))*(std::get<2>(u.deriv_value)) - std::get<0>(result.deriv_value)*(tensor_product_with_self(std::get<1>(u.deriv_value)));
	}

	static void cos(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<1, level, sparse_mode, num_vars, T>::cos(u, result);
		//std::get<2>(result.deriv_value) = -cxsc::sin(std::get<0>(u.deriv_value))*(std::get<2>(u.deriv_value)) + cxsc::cos(std::get<0>(u.deriv_value))*(tensor_product_with_self(std::get<1>(u.deriv_value)));
		std::get<2>(result.deriv_value) = -cxsc::sin(std::get<0>(u.deriv_value))*(std::get<2>(u.deriv_value)) - std::get<0>(result.deriv_value)*(tensor_product_with_self(std::get<1>(u.deriv_value)));
	}

	static bool max(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			const adhc_ari<level, sparse_mode, num_vars, T> &v,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		if (adhc_operations<1, level, sparse_mode, num_vars, T>::max(u, v, result)) return true;
		//std::get<2>(result.deriv_value) = std::get<2>(u.deriv_value) | std::get<2>(v.deriv_value);
		compute_Hesse_matrix_of_max (std::get<1>(u.deriv_value), std::get<1>(v.deriv_value),
					     std::get<2>(u.deriv_value), std::get<2>(v.deriv_value),
					     std::get<2>(result.deriv_value));
		return false;
	}

	static bool max(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			const cxsc::real &r,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		if (adhc_operations<1, level, sparse_mode, num_vars, T>::max(u, r, result)) return true;
		compute_Hesse_matrix_of_max_with_const (std::get<1>(u.deriv_value),
							std::get<2>(u.deriv_value),
							std::get<2>(result.deriv_value));
		return false;
	}
};


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
class adhc_operations<1, level, sparse_mode, num_vars, T> {

  public:
	static void init(adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::init(u, x);
		using deriv_types_list = typename adhc_ari<level, sparse_mode, num_vars, T>::deriv_types_list;
		using cur_deriv_type = typename TL::TypeAt<deriv_types_list, 1>::Result;
		std::get<1>(u.deriv_value) = DerivFactory::create(num_vars, Type2Type<cur_deriv_type>());
		std::get<1>(u.deriv_value) = 0.0;
	}

	static void output(std::ostream &s, const adhc_ari<level, sparse_mode, num_vars, T> &u) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::output(s, u);
		s << "grad = deriv(" << 1 << ") = " << std::get<1>(u.deriv_value) << "\n";
	}

	static std::string to_string(const adhc_ari<level, sparse_mode, num_vars, T> &u) {
		return adhc_operations<0, level, sparse_mode, num_vars, T>::to_string(u) + "grad = deriv(1) = " + std::to_string(std::get<1>(u.deriv_value)) + "\n";
	}

	static void multiply (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			      const adhc_ari<level, sparse_mode, num_vars, T> &v,
			      adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::multiply(u, v, result);
		std::get<1>(result.deriv_value) = derivative_of_product<level, sparse_mode, num_vars, T, 1>(u, v);
	}

	/*----static void multiply (adhc_ari<level, sparse_mode, num_vars> &u, const adhc_ari<level, sparse_mode, num_vars> &v) {
		adhc_operations<0, level, sparse_mode, num_vars>::multiply(u, v);
		std::get<1>(u.deriv_value) = derivative_of_product<level, sparse_mode, num_vars, 1>(u, v);
	}*/

	static void divide (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			    const adhc_ari<level, sparse_mode, num_vars, T> &v,
			    adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::divide(u, v, result);
		//std::get<1>(result.deriv_value) = std::get<1>(u.deriv_value) / std::get<0>(v.deriv_value) -
		//     	std::get<0>(u.deriv_value) * std::get<1>(v.deriv_value) / cxsc::sqr(std::get<0>(v.deriv_value));
		typename adhc_types<sparse_mode, num_vars, T>::grad_type nominator = std::get<1>(u.deriv_value) -
			std::get<0>(result.deriv_value) * std::get<1>(v.deriv_value);
		safe_division (nominator, std::get<0>(v.deriv_value), std::get<1>(result.deriv_value));
		/*typename adhc_types<sparse_mode, num_vars>::grad_type term1, term2;
		safe_division (std::get<1>(u.deriv_value), std::get<0>(v.deriv_value), term1);
		safe_division (std::get<0>(u.deriv_value) * std::get<1>(v.deriv_value),
			       cxsc::sqr(std::get<0>(v.deriv_value)),
			       term2);
		std::get<1>(result.deriv_value) = term1 - term2;*/
	}

	static void sqr(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::sqr(u, result);
		std::get<1>(result.deriv_value) = 2.0*std::get<0>(u.deriv_value)*std::get<1>(u.deriv_value);
	}

	static void power(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			  const int n,
			  adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::power(u, n, result);
		std::get<1>(result.deriv_value) = n*adhc::power(std::get<0>(u.deriv_value), n - 1)*std::get<1>(u.deriv_value);
		//std::get<1>(result.deriv_value) = n*cxsc::Power(std::get<0>(u.deriv_value), n - 1)*std::get<1>(u.deriv_value);
	}

	static void exp(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::exp(u, result);
		//std::get<1>(result.deriv_value) = cxsc::exp(std::get<0>(u.deriv_value))*std::get<1>(u.deriv_value);
		std::get<1>(result.deriv_value) = std::get<0>(result.deriv_value)*std::get<1>(u.deriv_value);
	}

	static void sin(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::sin(u, result);
		std::get<1>(result.deriv_value) = cxsc::cos(std::get<0>(u.deriv_value))*std::get<1>(u.deriv_value);
	}

	static void cos(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		adhc_operations<0, level, sparse_mode, num_vars, T>::cos(u, result);
		std::get<1>(result.deriv_value) = -cxsc::sin(std::get<0>(u.deriv_value))*std::get<1>(u.deriv_value);
	}

	static bool max (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			 const adhc_ari<level, sparse_mode, num_vars, T> &v,
			 adhc_ari<level, sparse_mode, num_vars, T> &result) {
		if (adhc_operations<0, level, sparse_mode, num_vars, T>::max(u, v, result)) return true;
		std::get<1>(result.deriv_value) = std::get<1>(u.deriv_value) | std::get<1>(v.deriv_value);
		return false;
		/*if (std::get<1>(u.deriv_value) != 0.0 || std::get<1>(v.deriv_value) != 0.0) {
			std::get<1>(result.deriv_value) = std::get<1>(u.deriv_value) | std::get<1>(v.deriv_value) | 0.0;
		}
		else ;//??????????????????????????*/
	}

	static bool max (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			 const cxsc::real &r,
			 adhc_ari<level, sparse_mode, num_vars, T> &result) {
		if (adhc_operations<0, level, sparse_mode, num_vars, T>::max(u, r, result)) return true;
		std::get<1>(result.deriv_value) = hull_with_zero (std::get<1>(u.deriv_value));
		return false;
	}
};


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
class adhc_operations<0, level, sparse_mode, num_vars, T> {
  public:
	static void init(adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
		std::get<0>(u.deriv_value) = x;
		//std::cout << "initializing: zero\t" << std::get<0>(u.deriv_value) << "\n";
	}

	static void output(std::ostream &s, const adhc_ari<level, sparse_mode, num_vars, T> &u) {
		s << "fun_val = " << std::get<0>(u.deriv_value) << "\n";
	}

	static std::string to_string(const adhc_ari<level, sparse_mode, num_vars, T> &u) {
		return "fun_val = " + std::to_string(std::get<0>(u.deriv_value)) + "\n";
	}

	static void multiply (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			      const adhc_ari<level, sparse_mode, num_vars, T> &v,
			      adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = std::get<0>(u.deriv_value) * std::get<0>(v.deriv_value);
	}

	/*----static inline void multiply(adhc_ari<level, sparse_mode, num_vars> &u, const adhc_ari<level, sparse_mode, num_vars> &v) {
		std::get<0>(u.deriv_value) *= std::get<0>(v.deriv_value);
	}*/

	static void divide (const adhc_ari<level, sparse_mode, num_vars, T> &u,
			    const adhc_ari<level, sparse_mode, num_vars, T> &v,
			    adhc_ari<level, sparse_mode, num_vars, T> &result) {
		safe_division (std::get<0>(u.deriv_value), std::get<0>(v.deriv_value), std::get<0>(result.deriv_value));
		/*if (cxsc::Inf(std::get<0>(v.deriv_value)) > 0.0 || cxsc::Sup(std::get<0>(v.deriv_value)) < 0.0) {
			// the divisor does not contain zero
			std::get<0>(result.deriv_value) = std::get<0>(u.deriv_value) / std::get<0>(v.deriv_value);
		}
		else {
			;
			//...
		}*/
	}

	static void sqr(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = cxsc::sqr(std::get<0>(u.deriv_value));
	}

	static void power(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			  const int n,
		    	  adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = adhc::power(std::get<0>(u.deriv_value), n);
		//std::get<0>(result.deriv_value) = cxsc::Power(std::get<0>(u.deriv_value), n);
	}

	static void exp(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = cxsc::exp(std::get<0>(u.deriv_value));
	}

	static void sin(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = cxsc::sin(std::get<0>(u.deriv_value));
	}

	static void cos(const adhc_ari<level, sparse_mode, num_vars, T> &u,
		    	adhc_ari<level, sparse_mode, num_vars, T> &result) {
		std::get<0>(result.deriv_value) = cxsc::cos(std::get<0>(u.deriv_value));
	}

	static bool max(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			const adhc_ari<level, sparse_mode, num_vars, T> &v,
			adhc_ari<level, sparse_mode, num_vars, T> &result) {
		// return value = should the procedure be stopped (true) or continued for higher drivatives (false)
		// return value: 1 - result = u, 2 - result = v, 3 - result uses both
		if (cxsc::Inf(std::get<0>(u.deriv_value)) > cxsc::Sup(std::get<0>(v.deriv_value))) {
			result = u;
			return true;
			//std::get<0>(result.deriv_value) = std::get<0>(u.deriv_value);
			//return 1;
		}
		if (cxsc::Inf(std::get<0>(v.deriv_value)) > cxsc::Sup(std::get<0>(u.deriv_value))) {
			result = v;
			return true;
			//std::get<0>(result.deriv_value) = std::get<0>(v.deriv_value);
			//return 2;
		}
		std::get<0>(result.deriv_value) = cxsc::interval(adhc::max(Inf(std::get<0>(u.deriv_value)),
									   Inf(std::get<0>(v.deriv_value))));
		cxsc::SetSup (std::get<0>(result.deriv_value),
			      adhc::max(Sup(std::get<0>(u.deriv_value)), Sup(std::get<0>(v.deriv_value))));
		return false;
	}

	static bool max(const adhc_ari<level, sparse_mode, num_vars, T> &u,
			const cxsc::real &r,
			adhc_ari<level, sparse_mode, num_vars, T> &result) {
		// return value = should the procedure be stopped (true) or continued for higher drivatives (false)
		if (cxsc::Inf(std::get<0>(u.deriv_value)) > r) {
			result = u;
			return true;
		}
		if (cxsc::Sup(std::get<0>(u.deriv_value)) < r) {
			result = r;
			return true;
		}
		std::get<0>(result.deriv_value) = T(adhc::max(Inf(std::get<0>(u.deriv_value)), r));
		cxsc::SetSup (std::get<0>(result.deriv_value), adhc::max(Sup(std::get<0>(u.deriv_value)), r));
		return false;
	}
};


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, std::ostream >::type &
//std::ostream & 
operator << (std::ostream &s, const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	adhc_operations<level, level, sparse_mode, num_vars, T>::output(s, u);
	return s;
}


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator + (const adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
	adhc_ari<level, sparse_mode, num_vars, T> result;

	adhc_operations_1<level, level, sparse_mode, num_vars, T>::add(u, v, result);
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator + (const adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
	adhc_ari<level, sparse_mode, num_vars, T> result = u;
	std::get<0>(result.deriv_value) += x;
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator + (const adhc_ari<level, sparse_mode, num_vars, T> &u, const cxsc::real &x) {
	adhc_ari<level, sparse_mode, num_vars, T> result = u;
	std::get<0>(result.deriv_value) += x;
	return result;
}


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator += (adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {

	adhc_operations_1<level, level, sparse_mode, num_vars, T>::add(u, v);
	return u;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator += (adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
	std::get<0>(u.deriv_value) += x;
	return u;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator += (adhc_ari<level, sparse_mode, num_vars, T> &u, const cxsc::real &x) {
	std::get<0>(u.deriv_value) += x;
	return u;
}


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator - (const adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
	adhc_ari<level, sparse_mode, num_vars, T> result;

	adhc_operations_1<level, level, sparse_mode, num_vars, T>::sub(u, v, result);
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator - (const adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
	adhc_ari<level, sparse_mode, num_vars, T> result = u;
	std::get<0>(result.deriv_value) -= x;
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator - (const adhc_ari<level, sparse_mode, num_vars, T> &u, const cxsc::real &x) {
	adhc_ari<level, sparse_mode, num_vars, T> result = u;
	std::get<0>(result.deriv_value) -= x;
	return result;
}


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator -= (adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {

	adhc_operations_1<level, level, sparse_mode, num_vars, T>::sub(u, v);
	return u;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator -= (adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
	std::get<0>(u.deriv_value) -= x;
	return u;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator -= (adhc_ari<level, sparse_mode, num_vars, T> &u, const cxsc::real &x) {
	std::get<0>(u.deriv_value) -= x;
	return u;
}


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator - (const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	adhc_ari<level, sparse_mode, num_vars, T> result;
	adhc_operations_1<level, level, sparse_mode, num_vars, T>::negate (u, result);
	return result;
}


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator * (const adhc_ari<level, sparse_mode, num_vars, T> &u,
	    const adhc_ari<level, sparse_mode, num_vars, T> &v) {
	adhc_ari<level, sparse_mode, num_vars, T> result;

	adhc_operations<level, level, sparse_mode, num_vars, T>::multiply(u, v, result);
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator * (const adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
	adhc_ari<level, sparse_mode, num_vars, T> result = u;
	adhc_operations_1<level, level, sparse_mode, num_vars, T>::multiply(result, x);
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator * (const adhc_ari<level, sparse_mode, num_vars, T> &u, const cxsc::real &x) {
	adhc_ari<level, sparse_mode, num_vars, T> result = u;
	adhc_operations_1<level, level, sparse_mode, num_vars, T>::multiply(result, x);
	return result;
}


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator *= (adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
	adhc_ari<level, sparse_mode, num_vars, T> result;
	adhc_operations<level, level, sparse_mode, num_vars, T>::multiply(u, v, result);
	u = result;
	return u;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator *= (adhc_ari<level, sparse_mode, num_vars, T> &u, const T &x) {
	adhc_operations<level, level, sparse_mode, num_vars, T>::multiply(u, x);
	//std::get<0>(u.deriv_value) *= x;
	return u;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type &
operator *= (adhc_ari<level, sparse_mode, num_vars, T> &u, const cxsc::real &x) {
	adhc_operations<level, level, sparse_mode, num_vars, T>::multiply(u, x);
	//std::get<0>(u.deriv_value) *= x;
	return u;
}


//*************************************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator / (const adhc_ari<level, sparse_mode, num_vars, T> &u,
	    const adhc_ari<level, sparse_mode, num_vars, T> &v) {
	adhc_ari<level, sparse_mode, num_vars, T> result;

	adhc_operations<level, level, sparse_mode, num_vars, T>::divide(u, v, result);
	return result;
}


//*************************************************************

// symmetry of arithmetic operations of adhc_ari and a real or an interval

template<typename TT, int level, SparsityLevel sparse_mode, int num_vars, typename T>
inline
//typename std::enable_if<!std::is_base_of<adhc_ari<level, sparse_mode, num_vars>, T>::value,
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator + (const TT &x, const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	//std::cout << "zamiana kolejnosci\n";
	return u + x;
}

template<typename TT, int level, SparsityLevel sparse_mode, int num_vars, typename T>
inline 
//typename std::enable_if<!std::is_base_of<adhc_ari<level, sparse_mode, num_vars>, T>::value,
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator - (const TT &x, const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	return -u + x;
}

template<typename TT, int level, SparsityLevel sparse_mode, int num_vars, typename T>
inline 
//typename std::enable_if<!std::is_base_of<adhc_ari<level, sparse_mode, num_vars>, T>::value,
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
operator * (const TT &x, const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	return u*x;
}


//*********************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
sqr(const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	adhc_ari<level, sparse_mode, num_vars, T> result;

	adhc_operations<level, level, sparse_mode, num_vars, T>::sqr(u, result);
	return result;
}


//*********************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
power(const adhc_ari<level, sparse_mode, num_vars, T> &u, const int n) {
	adhc_ari<level, sparse_mode, num_vars, T> result;
	adhc_operations<level, level, sparse_mode, num_vars, T>::power(u, n, result);
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
exp(const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	adhc_ari<level, sparse_mode, num_vars, T> result;
	adhc_operations<level, level, sparse_mode, num_vars, T>::exp(u, result);
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
sin(const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	adhc_ari<level, sparse_mode, num_vars, T> result;
	adhc_operations<level, level, sparse_mode, num_vars, T>::sin(u, result);
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
cos(const adhc_ari<level, sparse_mode, num_vars, T> &u) {
	adhc_ari<level, sparse_mode, num_vars, T> result;
	adhc_operations<level, level, sparse_mode, num_vars, T>::cos(u, result);
	return result;
}


//*********************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
max(const adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
	adhc_ari<level, sparse_mode, num_vars, T> result;
	adhc_operations<level, level, sparse_mode, num_vars, T>::max(u, v, result);
	return result;
}


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
max(const adhc_ari<level, sparse_mode, num_vars, T> &u, const cxsc::real &r) {
	adhc_ari<level, sparse_mode, num_vars, T> result;
	adhc_operations<level, level, sparse_mode, num_vars, T>::max(u, r, result);
	return result;
}


/*template<int level, SparsityLevel sparse_mode, int num_vars, typename T1, typename T2>
adhc_ari<level, sparse_mode, num_vars> min (const T1 &u, const T2 &v) {
	return -max<level, sparse_mode, num_vars>(-u, -v);
}*/

template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
adhc_ari<level, sparse_mode, num_vars, T> min (const adhc_ari<level, sparse_mode, num_vars, T> &u, const cxsc::real &v) {
	return -max<level, sparse_mode, num_vars, T>(-u, -v);
}


//*********************************************


template<int level, SparsityLevel sparse_mode, int num_vars, typename T>
typename std::enable_if<level >= 0, adhc_ari<level, sparse_mode, num_vars, T> >::type
intersection (const adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
	adhc_ari<level, sparse_mode, num_vars, T> result;

	adhc_operations_1<level, level, sparse_mode, num_vars, T>::intersection(u, v, result);
	return result;
}


} // end of namespace adhc

#endif

