
/*#include <civector.hpp>
#include <cimatrix.hpp>
#include <scivector.hpp>
#include <scimatrix.hpp>
#include <l_ivector.hpp>
#include <l_imatrix.hpp>*/

#include "adhc.hpp"

using namespace adhc;

const int level = 2;
const int num_vars = 3;

//#define SPARSITY SparsityLevel::dense
//#define SPARSITY SparsityLevel::sparse
#define SPARSITY SparsityLevel::highly_sparse

using diff_ari = adhc_ari<level, SPARSITY, num_vars>;

using hc_ari = adhc_ari<-1, SPARSITY, num_vars>;

template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f(const adhc_ari<lev, sparse_mode, n> &x) {
	//return sqr(x) - 2.0*x;
	//diff_ari result;
	adhc_ari<lev, sparse_mode, n> result;
	/*result = sqr(x);
	result -= 2.0*x;
	return result;*/
	//adhc_ari<lev, sparse_mode, n> &result = adhc_ari<lev, sparse_mode, n>::get_reference_or_copy (adhc_ari<lev, sparse_mode, n>());
	//result = x + 1.0;
	//result = sqr(x) + x + 1.0;
	//result = 1.0 - 2.0*x + sqr(x);
	result = max(x - 1.0, -x + 1.0);
	//result = sqr(x) - x*2.0;//2.0*x;
	//result = sqr(x + 1.0);
	//result = sqr(x);
	//result = x*x;
	//result = x*x*x;
	//result = 1.0 + 2.0*x + x*x;
	//result = sqr(x + cxsc::interval(-1.0));
	//result = (x + (-1.0))*sqr(x + (-1.0));
	//result = -sqr(sqr(x));
	return result;
}


template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> fun(const adhc_vector<lev, sparse_mode, n> &x) {
	//return sqr(x[1]) + x[1] + x[2] + exp(x[3]) - 7.0*x[3] + 44.0;
	//return sqr(x[1]);
	//return x[1] + x[2] + x[3];
	adhc_ari<lev, sparse_mode, n> result;
	//-result = sqr(x[1]) + sqr(x[2]);
	//-return result;
	/*adhc_ari<lev, sparse_mode, n> *p_result = new adhc_ari<lev, sparse_mode, n>;
	adhc_ari<lev, sparse_mode, n> &result = *p_result;*/
	//adhc_ari<lev, sparse_mode, n> &result = adhc_ari<lev, sparse_mode, n>::get_reference_or_copy (adhc_ari<lev, sparse_mode, n>(x[1]*x[2]));
	//result = x[1]*x[2];
	//for (int i = 1; i <= n; ++i) result += (x[i] + 1.0);
	result = x[1];
	for (int i = 2; i <= n; ++i) result += x[i];
	for (int i = 1; i <= n; ++i) result += sqr(x[i]);
	result += 7.0;
	return result;
	//result += sqr(x[1]);
	//result += (x[1] - x[2]);
	result -= power(x[3], 7);
	//result += exp(x[1]);
	//return sqr(result);
	return result;
	//return result * (x[1] - x[2]);
	/*{
		adhc_ari<lev, sparse_mode, n> result2;
		result2 = result + x[1];
		std::cout << result2;
		return result2;
	}*/
	//return result + sqr(x[1]);
	//return (x[1] - x[2]) + result;
	//return -(x[1] + x[2])*(x[1] - x[2]) + 1.0 + sqr(x[3]);
	//return x[1]*x[2]*x[3]*sqr(x[1]);
	//return x[1]*x[2] - sqr(x[1]) + x[2] + sqr(x[2]);
	//return sqr(x[1]) - sqr(x[2]);
	//return -sqr(sqr(x[1]));
	//return -sqr(x[1]);
	//return x[1] + sqr(x[2]);
	//return x[1] + x[2];
}


template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f1(const adhc_vector<lev, sparse_mode, n> &x) {
	return x[1] + x[1]*x[2] + x[2];
}

template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f2(const adhc_vector<lev, sparse_mode, n> &x) {
	return intersection(x[1]*(1.0 + x[2]) + x[2], x[1] + (x[1] + 1.0)*x[2]);
}


template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f_div(const adhc_vector<lev, sparse_mode, n> &x) {
	return x[1]/x[2];
}


template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f_ex(const adhc_vector<lev, sparse_mode, n> &x) {
	//return x[1]*x[2] - 1.0;
	//return sqr(x[1]) + sqr(x[2]) - 1.0;
	adhc_ari<lev, sparse_mode, n> result/*;
	//result = sqr(x[1]) + sqr(x[2]) - 1.0;
	result*/ = sqr(x[1]);
	result += sqr(x[2]);
	result -= 1.0;
	return result;
}

template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f_example(const adhc_vector<lev, sparse_mode, n> &x) {
	adhc_ari<lev, sparse_mode, n> result;
	for (int i = 1; i <= n; ++i) result += sqr(x[i]);
	return result;
}

int main() {
	cxsc::interval x (1.0, 2.0);

	diff_ari x_ (x);
	std::cout << x_;
	diff_ari y_ = f(x_);
	std::cout << y_;

	std::cout << "variable:\n";

	x_ = diff_ari::create_variable (1, x);
	std::cout << x_;

	y_ = f(x_);
	std::cout << y_;

	/*std::cout << "HC:\n";

	hc_ari other_x_ = hc_ari::create_variable (1, x);
	//std::cout << other_x_;
	std::cout << other_x_;
	std::cout << '\n';
	hc_ari other_y_ = f(other_x_);
	std::cout << other_y_;*/

	std::cout << "\nno derivs:\n";

	adhc_ari<0, SPARSITY, num_vars> xx_ (x);
	std::cout << xx_;
	adhc_ari<0, SPARSITY, num_vars> yy_ = f(xx_);
	std::cout << yy_;

	if (num_vars > 1)
	{
		std::cout << "vector:\n";
		adhc_fun<level, SPARSITY, num_vars>::adhc_fun_ptr function = fun<level, SPARSITY, num_vars>;
		cxsc::ivector x(3);
		x[1] = cxsc::interval(0.0, 1.0); x[2] = cxsc::interval(1.0, 3.0);
		//x[1] = x[2] = cxsc::interval(1.0, 3.0);
		adhc_vector<level, SPARSITY, num_vars> x_ (x);
		std::cout << "x[1]:\n" << x_[1];
		std::cout << "x[2]:\n" << x_[2];
		std::cout << "y:\n";
		y_ = function(x_);
		//y_ = fun<level, SPARSITY, num_vars>(x_);
		std::cout << y_;

		std::cout << "HC:\n";

		std::cout << "Creating x_hc\n";
		//adhc_vector<-1, SPARSITY, num_vars> *x_hc_ptr = new adhc_vector<-1, SPARSITY, num_vars> (x);
		//adhc_vector<-1, SPARSITY, num_vars> &x_hc = *x_hc_ptr;
		adhc_vector<-1, SPARSITY, num_vars> x_hc (x);
		std::cout << "Creating y_hc\n";
		//adhc_ari<-1, SPARSITY, num_vars> * y_hc_ptr = new adhc_ari<-1, SPARSITY, num_vars> (fun<-1, SPARSITY, num_vars>(x_hc));
		/*adhc_ari<-1, SPARSITY, num_vars> * y_hc_ptr = new adhc_ari<-1, SPARSITY, num_vars> (fun(x_hc));
		std::cout << "Assigning y_hc_ptr to a reference.\n";
		adhc_ari<-1, SPARSITY, num_vars> &y_hc = *y_hc_ptr;*/
		adhc_ari<-1, SPARSITY, num_vars> y_hc = fun(x_hc);
		std::cout << "x[1]:\n" << x_hc[1];
		std::cout << "\nx[2]:\n" << x_hc[2];
		std::cout << "\ny:\n";
		//adhc_ari<-1, SPARSITY, num_vars> * y_hc_ptr = new adhc_ari<-1, SPARSITY, num_vars> (fun<-1, SPARSITY, num_vars>(x_hc));
		//adhc_ari<-1, SPARSITY, num_vars> * y_hc_ptr = (fun<-1, SPARSITY, num_vars>(x_hc)).shallow_copy();
		//adhc_ari<-1, SPARSITY, num_vars> &y_hc = *y_hc_ptr;
		std::cout << y_hc << '\n';
		evaluate_nodes (&y_hc, x);
		cxsc::intvector which(3);
		int result = hc_enforce (&y_hc, x, which, cxsc::interval(1.0, 2.0));
		std::cout << "result of HC: " << result << "\n";
		std::cout << "x = " << x << "\n\n";
		//delete y_hc_ptr;
		//delete x_hc_ptr;
		//return 0;
	}

	/*std::cout << "Factorial:\n";
	std::cout << factorial<0>::value << '\n';
	std::cout << factorial<1>::value << '\n';
	std::cout << factorial<2>::value << '\n';
	std::cout << factorial<3>::value << '\n';
	std::cout << factorial<4>::value << '\n';
	std::cout << '\n';
	std::cout << factorial_product<4, 2>::value << '\n';
	std::cout << '\n';
	std::cout << binom_coef<4, 4>::value << '\n';
	std::cout << binom_coef<4, 3>::value << '\n';
	std::cout << binom_coef<4, 2>::value << '\n';
	std::cout << binom_coef<4, 1>::value << '\n';
	std::cout << binom_coef<4, 0>::value << '\n';*/
	{
		const int N = 4;
		cxsc::imatrix A_pom(N, N);
		A_pom = 0.0;
		for (int i = 1; i <= N; ++i) A_pom[i][i] = cxsc::interval(double(i), double(i + 1));
		A_pom[1][2] = 1.0;
		cxsc::simatrix A(A_pom);
		std::cout << "A = " << A << '\n';
		std::cout << "A.values = ";
		for (int i = 0; i < A.values().size(); ++i) std::cout << A.values()[i] << " ";
		std::cout << "\nA.column_pointers = ";
		for (int i = 0; i < A.column_pointers().size(); ++i) std::cout << A.column_pointers()[i] << ' ';
		std::cout << "\nA.row_indices = ";
		for (int i = 0; i < A.row_indices().size(); ++i) std::cout << A.row_indices()[i] << ' ';
		//std::cout << "A.row_indices = " << A.row_indices() << '\n';
		//std::cout << "A.column_pointers = " << A.column_pointers() << '\n';
		std::cout << "\n";
	}
	/*{
		cxsc::interval x (1.0, 2.0);
		std::cout << "pow(" << x << ", 2) = " << pow(x, cxsc::interval(2.0)) << "\n";
		std::cout << "pow(" << x << ", 0.5) = " << pow(x, cxsc::interval(0.5)) << "\n";
		std::cout << "pow(" << x << ", -2) = " << pow(x, cxsc::interval(-2.0)) << "\n";
		std::cout << "power(" << x << ", -2) = " << power(x, -2) << "\n";
		std::cout << "Power(" << x << ", -2) = " << Power(x, -2) << "\n";
		x = cxsc::interval (-2.0, -1.0);
		std::cout << "pow(" << x << ", 2) = " << pow(x, cxsc::interval(2.0)) << "\n";
		std::cout << "power(" << x << ", -2) = " << power(x, -2) << "\n";
		std::cout << "Power(" << x << ", -2) = " << Power(x, -2) << "\n";
		std::cout << "pow(" << x << ", -2) = " << pow(x, cxsc::interval(-2.0)) << "\n";
		//std::cout << "pow(" << x << ", 0.5) = " << pow(x, cxsc::interval(0.5)) << "\n";
		x = cxsc::interval (-1.0, 1.0);
		std::cout << "pow(" << x << ", 2) = " << pow(x, cxsc::interval(2.0)) << "\n";
		//std::cout << "power(" << x << ", -2) = " << power(x, -2) << "\n";
		//std::cout << "Power(" << x << ", -2) = " << Power(x, -2) << "\n";
		//std::cout << "pow(" << x << ", -2) = " << pow(x, cxsc::interval(-2.0)) << "\n";
		//std::cout << "pow(" << x << ", 0.5) = " << pow(x, cxsc::interval(0.5)) << "\n";
	}*/
	{
		cxsc::ivector x(2);
		x[1] = cxsc::interval(-2.0, 3.0);
		x[2] = cxsc::interval(-3.0, 2.0);
		adhc_vector<level, SPARSITY, 2> x_ (x);
		adhc_ari<level, SPARSITY, 2> y1_ = f1(x_);
		adhc_ari<level, SPARSITY, 2> y2_ = f2(x_);
		std::cout << "f1: " << y1_;
		std::cout << "f2: " << y2_;
	}
	{
		cxsc::ivector x(2);
		x[1] = cxsc::interval(1.0, 2.0);
		x[2] = cxsc::interval(0.0, 1.0);
		adhc_vector<level, SPARSITY, 2> x_ (x);
		adhc_ari<level, SPARSITY, 2> y_ = f_div(x_);
		std::cout << "f_div: " << y_;
	}
	{
		cxsc::ivector x(2);
		x[1] = cxsc::interval(-1.0, 1.0);
		x[2] = cxsc::interval(0.5, 2.0);
		adhc_vector<-1, SPARSITY, 2> x_hc (x);
		std::cout << "\nx = " << x << "\n\n";
		//adhc_ari<-1, SPARSITY, 2> * y_hc_ptr = new adhc_ari<-1, SPARSITY, 2>;
		//-adhc_ari<-1, SPARSITY, 2> * y_hc_ptr = new adhc_ari<-1, SPARSITY, 2> (f_ex(x_hc));
		//*y_hc_ptr = f_ex(x_hc);
		//-auto & y_hc = *y_hc_ptr;
		adhc_ari<-1, SPARSITY, 2> y_hc = f_ex(x_hc);
		std::cout << "y_hc = " << y_hc << "\n\n";
		//std::cout << x_hc;
		evaluate_nodes (&y_hc, x);
		std::cout << "y_hc: ";
		std::cout << y_hc;
		cxsc::intvector which(2);
		int result = hc_enforce (&y_hc, x, which, cxsc::interval(0.0));
		std::cout << "\nresult of HC: " << result << "\n";
		std::cout << "x = " << x << "\n\n";
		result = hc_enforce (&y_hc, x, which, cxsc::interval(0.0));
		std::cout << "\nresult of HC: " << result << "\n";
		std::cout << "x = " << x << "\n\n";
		//delete y_hc_ptr;
	}
	{
		const int N = 8;
		cxsc::ivector x(N);
		x = 1.0;
		adhc_vector<1, SPARSITY, N> x_ (x);
		adhc_ari<1, SPARSITY, N> y_ = f_example(x_);
		std::cout << y_;
	}
	return 0;
}

