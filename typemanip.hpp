#if !defined __TYPEMANIP_HPP__
#define __TYPEMANIP_HPP__

template<typename T>
struct Type2Type {
	using Result = T;
};

#endif

