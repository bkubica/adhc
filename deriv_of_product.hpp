#if !defined __DERIV_OF_PRODUCT_HPP__
#define __DERIV_OF_PRODUCT_HPP__

#include "product.hpp"
#include "adhc_deriv_type.hpp"

//template<int level, SparsityLevel sparse_mode, int num_vars, typename T> class adhc_ari;

template<int level, SparsityLevel sparse_mode, int num_vars, typename T, int deriv_num, int summand_num>
class adhc_multiplication_auxiliary_operations {
  public:
	using deriv_type = typename adhc_deriv_type<deriv_num, sparse_mode, num_vars, T>::deriv_type;

	static deriv_type summand (const adhc_ari<level, sparse_mode, num_vars, T> &u,
				   const adhc_ari<level, sparse_mode, num_vars, T> &v) {
		//typename adhc_deriv_type<deriv_num, sparse_mode, num_vars>::deriv_type result;
		//return result*binom_coef<level, summand_num>::value;
		return tensor_product(tensor_product(std::get<summand_num>(u.deriv_value),
			std::get<deriv_num - summand_num>(v.deriv_value)),
			T(binom_coef<deriv_num, summand_num>::value));
	}

	static deriv_type sum_for_product (const adhc_ari<level, sparse_mode, num_vars, T> &u,
					   const adhc_ari<level, sparse_mode, num_vars, T> &v) {
		/*std::cout << "sum_for_product, summand_num = " << summand_num << '\n';
		std::cout << "\tfirst: " << std::get<summand_num>(u.deriv_value) << '\n';
		std::cout << "\tsecond: " << std::get<deriv_num - summand_num>(v.deriv_value) << '\n';
		std::cout << "\tcoef: " << binom_coef<level, summand_num>::value << '\n';
		std::cout << adhc_multiplication_auxiliary_operations<level, sparse_mode, num_vars, deriv_num, summand_num>::summand(u, v) << '\n';*/
		return adhc_multiplication_auxiliary_operations<level, sparse_mode, num_vars, T, deriv_num, summand_num>::summand(u, v) +
			adhc_multiplication_auxiliary_operations<level, sparse_mode, num_vars, T, deriv_num, summand_num - 1>::sum_for_product(u, v);
	}
};


template<int level, SparsityLevel sparse_mode, int num_vars, typename T, int deriv_num>
class adhc_multiplication_auxiliary_operations<level, sparse_mode, num_vars, T, deriv_num, 0> {
  public:
	using deriv_type = typename adhc_deriv_type<deriv_num, sparse_mode, num_vars, T>::deriv_type;

	static deriv_type sum_for_product (const adhc_ari<level, sparse_mode, num_vars, T> &u,
					   const adhc_ari<level, sparse_mode, num_vars, T> &v) {
		//std::cout << "ost: " << tensor_product(tensor_product(std::get<0>(u.deriv_value), std::get<deriv_num>(v.deriv_value)), T(binom_coef<level, 0>::value)) << '\n';
		return tensor_product(tensor_product(std::get<0>(u.deriv_value), std::get<deriv_num>(v.deriv_value)), T(binom_coef<deriv_num, 0>::value));
	}
};


template<int level, SparsityLevel sparse_mode, int num_vars, typename T, int deriv_num>
typename adhc_deriv_type<deriv_num, sparse_mode, num_vars, T>::deriv_type derivative_of_product (
		const adhc_ari<level, sparse_mode, num_vars, T> &u, const adhc_ari<level, sparse_mode, num_vars, T> &v) {
	return adhc_multiplication_auxiliary_operations<level, sparse_mode, num_vars, T, deriv_num, deriv_num>::sum_for_product(u, v);
	//typename adhc_deriv_type<deriv_num, sparse_mode, num_vars>::deriv_type result;
	//return result;
}

#endif

