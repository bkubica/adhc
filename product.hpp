#if !defined __PRODUCT_HPP__
#define __PRODUCT_HPP__

template<int n>struct factorial {
	static const int value = n*factorial<n - 1>::value;
};

template<> struct factorial<0> {
	static const int value = 1;
};



template<int n, int k>struct factorial_product {
	static const int value = n*factorial_product<n - 1, k>::value;
};

template<int k> struct factorial_product<k, k> {
	static const int value = 1;
};



template<int n, int k>struct binom_coef {
	static const int value = factorial_product<n, k>::value/factorial<n - k>::value;
};
#endif

