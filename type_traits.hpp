#include "typelist.hpp"


using interval_types = TL::TypeList<cxsc::interval, cxsc::cinterval, cxsc::l_interval>;
// cxsc::l_cinterval, cxsc::lx_interval, cxsc::lx_cinterval

using scalar_types = TL::TypeList<cxsc::real, cxsc::complex, cxsc::l_real, cxsc::interval, cxsc::cinterval, cxsc::l_interval>;
// cxsc::l_creal, cxsc::lx_real, cxsc::lx_creal
// cxsc::l_cinterval, cxsc::lx_interval, cxsc::lx_cinterval

using vector_types = TL::TypeList<cxsc::rvector, cxsc::srvector, cxsc::cvector, cxsc::scvector, cxsc::l_rvector, cxsc::ivector, cxsc::sivector, cxsc::civector, cxsc::scivector, cxsc::l_ivector>;

using matrix_types = TL::TypeList<cxsc::rmatrix, cxsc::srmatrix, cxsc::cmatrix, cxsc::scmatrix, cxsc::l_rmatrix, cxsc::imatrix, cxsc::simatrix, cxsc::cimatrix, cxsc::scimatrix, cxsc::l_imatrix>;


//*************************************************


template<class T>
struct underlying_point_type {
	using infimum_type = decltype(cxsc::Inf(std::declval<T>()));
	using nonreferenced_infimum_type = typename std::remove_reference<infimum_type>::type;
        using type = typename std::remove_cv<nonreferenced_infimum_type>::type;
	// gives, e.g., cxsc::real for cxsc::interval, cxsc::l_real for cxsc::l_interval, etc.
	// also cxsc::rvector for cxsc::ivector, etc.
	//
	// As Inf() returns typically a const reference, we need to strip these type modifiers.
	// remove_const() would probably be sufficient, but we use remove_cv(), just in case...
};


//*************************************************


template<class T, bool is_vector = false, bool is_matrix = false>
struct underlying_element_type_;

template<class T>
struct underlying_element_type_<T, true, false> {
	using type = decltype(std::declval<T>()[0]);
};

template<class T>
struct underlying_element_type_<T, false, true> {
	using type = decltype(std::declval<T>()[0][0]);
};


template<class T>
struct underlying_element_type {
	using elem_type = typename underlying_element_type_<T, TL::IndexOf<vector_types, T>::value != -1, TL::IndexOf<matrix_types, T>::value != -1>::type;
	using nonreferenced_elem_type = typename std::remove_reference<elem_type>::type;
        using type = typename std::remove_cv<nonreferenced_elem_type>::type;
};


//*************************************************


template<typename T> struct Types;
/*struct Types {
	using scalar_type = T;

	using dense_vector_type = T;
	using dense_matrix_type = T;
	using dense_tensor_type = T;

	using sparse_vector_type = T;
	using sparse_matrix_type = T;
	using sparse_tensor_type = T;

	static const bool has_sparse_types = false;
};*/


//*************************************************


template<>
struct Types<cxsc::interval> {
	using scalar_type = cxsc::interval;

	using dense_vector_type = cxsc::ivector;
	using dense_matrix_type = cxsc::imatrix;
	using dense_tensor_type = itensor;

	using sparse_vector_type = cxsc::sivector;
	using sparse_matrix_type = cxsc::simatrix;
	using sparse_tensor_type = sitensor;

	static const bool has_sparse_types = true;
};


template<>
struct Types<cxsc::cinterval> {
	using scalar_type = cxsc::cinterval;

	using dense_vector_type = cxsc::civector;
	using dense_matrix_type = cxsc::cimatrix;
	using dense_tensor_type = citensor;

	using sparse_vector_type = cxsc::scivector;
	using sparse_matrix_type = cxsc::scimatrix;
	using sparse_tensor_type = scitensor;

	static const bool has_sparse_types = true;
};


template<>
struct Types<cxsc::l_interval> {
	using scalar_type = cxsc::l_interval;

	using dense_vector_type = cxsc::l_ivector;
	using dense_matrix_type = cxsc::l_imatrix;
	using dense_tensor_type = l_itensor;

	using sparse_vector_type = dense_vector_type; //cxsc::sivector;
	using sparse_matrix_type = dense_matrix_type; //cxsc::simatrix;
	using sparse_tensor_type = dense_tensor_type; //sitensor; 

	static const bool has_sparse_types = false;
};


//*************************************************


template<>
struct Types<cxsc::real> {
	using scalar_type = cxsc::real;

	using dense_vector_type = cxsc::rvector;
	using dense_matrix_type = cxsc::rmatrix;
	using dense_tensor_type = rtensor;

	using sparse_vector_type = cxsc::srvector;
	using sparse_matrix_type = cxsc::srmatrix;
	using sparse_tensor_type = srtensor;

	static const bool has_sparse_types = true;
};


template<>
struct Types<cxsc::complex> {
	using scalar_type = cxsc::complex;

	using dense_vector_type = cxsc::cvector;
	using dense_matrix_type = cxsc::cmatrix;
	using dense_tensor_type = ctensor;

	using sparse_vector_type = cxsc::scvector;
	using sparse_matrix_type = cxsc::scmatrix;
	using sparse_tensor_type = sctensor;

	static const bool has_sparse_types = true;
};


template<>
struct Types<cxsc::l_real> {
	using scalar_type = cxsc::l_real;

	using dense_vector_type = cxsc::l_rvector;
	using dense_matrix_type = cxsc::l_rmatrix;
	using dense_tensor_type = l_itensor;

	using sparse_vector_type = dense_vector_type; //cxsc::srvector;
	using sparse_matrix_type = dense_matrix_type; //cxsc::srmatrix;
	using sparse_tensor_type = dense_tensor_type; //srtensor;

	static const bool has_sparse_types = false;
};

