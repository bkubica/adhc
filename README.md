# **ADHC** - An **Algorithmic Differentiation** and **Hull Consistency** enforcing library.
# It assumes the **C++11** standard, at least and is dependent on the **C-XSC** library.

============

1. Introduction.

ADHC (Algorithmic Differentiation and Hull-Consistency enforcing) is a C++
library, providing template classes for maintaining mathematical expressions.
Its primary goal is to support interval algorithms, but non-interval ones can
use it as well: at least the AD features.
The library is indexed by the *Numerical Algorithms Journal*, using the code *na60*.


2. License.

The software is distributed under the GPLv2 free software license.
Please, consult the COPYING file for the details.


3. Requirements.

The ADHC library depends on C-XSC, that used to be available at:
<http://www.xsc.de>
As this library has not been updated since 2014 (and it is, probably, not
compatible with the C++17 or newer standards), it has been forked by the
author. The survive-CXSC library is available at:
<https://gitlab.com/bkubica/survive-cxsc)>
Using survive-CXSC is strongly recommended; the author gives no warranty that
ADHC will work with the older counterpart of the library, or even he gives a warranty, it will not.
For instance, the sqr() function will not work for cxsc::complex type, and
cxsc::l_real, cxsc::l_interval, ... types are *not* thread-safe, for the old C-XSC.

If the (survive-)CXSC library is compiled in the form of a dynamically-linked
library, please make sure the proper file is added to the LD_LIBRARY_PATH
environment variable!

In its current version, ADHC requires a C++ compiler compatible with at least
C++14 standard. The author has tested it on GCC, version 11.1.0, under the
control of GNU/Linux operating system.


4. Compilation.

The library is header-only, so it does not require compilation itself.
Simply, please include \<adhc.hpp\> in your C++ files, to use ADHC in your
project.

The compilation system for the examples and tests is a bit primitive, which
will hopefully be improved in the future versions.
Currently, there is a Makefile, in which you can set the following variables:

 - CXX - the C++ compiler to be used (g++, icc, etc.).
 - XSC_DIR - the directory, where C-XSC or (preferably) survive-CXSC library
is installed. It is assumed that  this directory contains (at least) two
subdirectories: include/ and lib/.
 - TARGETS - only modified when you do not want to compile some of the
unit test files (see the next point for details).


5. Examples and unit tests.

The example Makefile produces three files: examples, tests and gtests.

a) The file examples.cpp

This file, in its current form, illustrates, how to use the ADHC library, by
performing a few operations (all of them, using the interval arithmetic):
 - computing the value of function f(x) = |x - 1| = max(x - 1.0, -x + 1.0) on
   interval x \in [1, 2],
 - computing the value, gradient, and Hesse matrix of the function fun(x1, x2,
   x3) = x1 + x2 + x3 + x1^2 + x2^2 + x3^2 + 7.0
 - creating the expression tree for the aforementioned function fun(), and
   performing HC enforcing on an example domain,
 - computing the value, gradient, and Hesse matrix for the following
   functions:
   f1(x1, x2) = x1 + x1 \* x2 + x2
   f2(x1, x2) = intersection(x1 \* (1 + x2) + x2, x1 + (x1 + 1) \* x2)
   f_div(x1, x2) = x1/x2
 - creating the expression tree, and performing HC enforcing for the function
   f_ex(x1, x2) = x1^2 + x2^2 - 1, x1 \in [-1, 1], x2 \in [0.5, 2]
 - computing the value and gradient, of the function f_example = sum_{xi}
   xi^2, for i = 1, ..., 8, and xi \in [1, 1]
There are other commented examples, as well. Uncomment them, to check their
behavior.

b) The files tests.cpp and gtests.cpp

These files contain unit tests for libraries boost::test and GoogleTest (gtest),
respectively. If you do not have any of them, please disable the build of the
related file(s). Disabling can simply be done by removing the proper file
from the list in TARGETS variable defined in the Makefile.

You can also check for the HIBA_USNE solver:
<https://gitlab.com/bkubica/hiba_usne>
that is also using the ADHC library, illustrating some of its use-cases.

