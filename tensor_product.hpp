#if !defined __TENSOR_PRODUCT_HPP__
#define __TENSOR_PRODUCT_HPP__

#include<ivector.hpp>

template<class T>
typename std::enable_if<TL::IndexOf<adhc::scalar_types, T>::value != -1, T>::type tensor_product (const T &x, const T &y) {
	return x*y;
}

/*template<class T1, class T2>
//typename std::enable_if<TL::IndexOf<adhc::scalar_types, T>::value != -1, T>::type 
auto tensor_product (const typename std::enable_if<TL::IndexOf<adhc::scalar_types, T1>::value != -1, T1>::type &x,
		     const typename std::enable_if<TL::IndexOf<adhc::scalar_types, T2>::value != -1, T2>::type &y) {
	return x*y;
}*/

template<class T>
typename std::enable_if<TL::IndexOf<adhc::vector_types, T>::value != -1, typename adhc::Types<typename underlying_element_type<T>::type>::dense_matrix_type>::type
tensor_product (const T &x, const T &y) {
	const int n = VecLen(x);
	const int m = VecLen(y);
	typename adhc::Types<typename underlying_element_type<T>::type>::dense_matrix_type result(n, m);
	for (int i = 1; i <= n; ++i) {
		result[i] = x[i]*y;
		//for (int j = 1; j <= m; ++j) result[i][j] = x[i]*y[j];
	}
	return result;
}

/*cxsc::sivector tensor_product (const cxsc::sivector &x, const cxsc::interval &z) {
	return x*z;
}

cxsc::sivector tensor_product (const cxsc::interval &x, const cxsc::sivector &z) {
	return x*z;
}*/

//cxsc::simatrix tensor_product (const cxsc::sivector &x, const cxsc::sivector &y) {
cxsc::imatrix tensor_product (const cxsc::sivector &x, const cxsc::sivector &y) {
	/*cxsc::ivector x_dense(x);
	cxsc::ivector y_dense(y);
	cxsc::imatrix result = tensor_product(x_dense, y_dense);
	return cxsc::simatrix(result);*/
	cxsc::ivector y_dense(y);
	cxsc::imatrix result(VecLen(x), VecLen(x));
	result = 0.0;
	const int n = x.values().size();
	//std::cout << "n = " << n << "\n";
	//std::cout << x.row_indices().size() << "\n";;
	//std::cout << x.get_nnz() << "\n";
	//std::cout << "VecLen: " << VecLen(x) << ", " << VecLen(y) << "\n";
	for (int i = 0; i < n; ++i) {
		//std::cout << "i = " << i << "\n";
		//std::cout << "x = " << x.values()[i] << "\n";
		//std::cout << "ind = " << x.row_indices()[i] << "\n";
		const int row_num = x.row_indices()[i];
		result[row_num + 1] = x.values()[i]*y_dense; //cxsc!!!
	}
	//return cxsc::simatrix(result);
	return result;
}

/*cxsc::simatrix tensor_product (const cxsc::simatrix &A, const cxsc::interval &z) {
	return A*z;
}

itensor tensor_product (const itensor &x, const cxsc::interval &z);

cxsc::simatrix tensor_product (const cxsc::interval &z, const cxsc::simatrix &A) {
	return A*z;
}

template<class T>
T tensor_product (const cxsc::l_interval &x, const T &y) {
	return x*y;
}*/

template<typename scalar_T, class T>
inline typename std::enable_if<TL::IndexOf<scalar_types, scalar_T>::value != -1, T>::type tensor_product (const scalar_T &z, const T &x) {
	return x*z;
}

template<typename T, class scalar_T>
inline typename std::enable_if<TL::IndexOf<scalar_types, scalar_T>::value != -1, T>::type tensor_product (const T &x, const scalar_T &z) {
	return x*z;
}

/*template<typename scalar_T, class T>
inline T tensor_product (const typename std::enable_if<TL::IndexOf<TL::TypeList<cxsc::sivector, cxsc::simatrix>, scalar_T>::value != -1, scalar_T>::type &z, const T &x) {
	return tensor_product(x, z);
}*/

itensor tensor_product (const cxsc::imatrix &A, const cxsc::ivector &y);

sitensor tensor_product (const cxsc::simatrix &A, const cxsc::ivector &y);

sitensor tensor_product (const cxsc::simatrix &A, const cxsc::sivector &y);

itensor tensor_product (const cxsc::imatrix &A, const cxsc::imatrix &y);

sitensor tensor_product (const cxsc::simatrix &A, const cxsc::simatrix &y);

itensor tensor_product (const itensor &A, const cxsc::ivector &y);

itensor tensor_product (const itensor &A, const cxsc::imatrix &B);

itensor tensor_product (const itensor &A, const itensor &B);

sitensor tensor_product (const sitensor &A, const cxsc::ivector &y);

sitensor tensor_product (const sitensor &A, const cxsc::sivector &y);

sitensor tensor_product (const sitensor &A, const cxsc::simatrix &B);

sitensor tensor_product (const sitensor &A, const sitensor &B);

//**************************************************************

template<class T>
typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1, T>::type tensor_product_with_self (const T &x) {
	return cxsc::sqr(x);
}

template<class T>
typename std::enable_if<TL::IndexOf<adhc::vector_types, T>::value != -1, typename adhc::Types<typename underlying_element_type<T>::type>::dense_matrix_type>::type
tensor_product_with_self (const T &x) {
	const int n = VecLen(x);
	typename adhc::Types<typename underlying_element_type<T>::type>::dense_matrix_type result(n, n);
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j < i; ++j) result[i][j] = result[j][i] = x[i]*x[j];
		result[i][i] = cxsc::sqr(x[i]);
	}
	return result;
}

//cxsc::simatrix tensor_product_with_self (const cxsc::sivector &x) {
cxsc::imatrix tensor_product_with_self (const cxsc::sivector &x) {
	/*cxsc::ivector x_dense(x);
	cxsc::imatrix result = tensor_product_with_self(x_dense);
	return cxsc::simatrix(result);*/
	const int n = x.get_nnz();
	cxsc::imatrix result(VecLen(x), VecLen(x));
	result = 0.0;
	for (int i = 0; i < n; ++i) {
		const int x_pos = x.row_indices()[i];
		for (int j = 0; j < i; ++j) {
			const int y_pos = x.row_indices()[j];
			result[x_pos + 1][y_pos + 1] = result[y_pos + 1][x_pos + 1] = x.values()[i]*x.values()[j];
		}
		result[x_pos + 1][x_pos + 1] = cxsc::sqr(x.values()[i]);
	}
	//return cxsc::simatrix(result);
	return result;
}

itensor tensor_product_with_self (const cxsc::imatrix &A);

sitensor tensor_product_with_self (const cxsc::simatrix &A);

itensor tensor_product_with_self (const itensor &A);

sitensor tensor_product_with_self (const sitensor &A);

//**************************************************************

template<class T>
typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1, T>::type transp(const T &x) {
	return x;
}

#endif

