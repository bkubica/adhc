\documentclass[a4paper,11pt]{article}

%\usepackage{amstex}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage[latin2]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
%\usepackage{amsbsy}
\usepackage{graphics}
%\usepackage{epic}
%\usepackage{eepic}
\usepackage{url}
\frenchspacing
\renewcommand{\le}{\leqslant}
\renewcommand{\ge}{\geqslant}
\newcommand{\then}{\Rightarrow}

%\renewcommand{\baselinestretch}{2}

\newcommand{\po}{\mbox{s.t.}}

\newcommand{\R}{\ensuremath\mathbb{R}}

\DeclareMathOperator{\oczek}{\mathbb{E}}
\newcommand{\E}{\oczek\displaylimits}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{example}{Example}[section]


%\pagestyle{empty}
%\addtolength{\hoffset}{0.7cm}
\addtolength{\topmargin}{-1.5cm}
\addtolength{\textheight}{3cm}
\addtolength{\oddsidemargin}{-1.5cm}
\addtolength{\evensidemargin}{-1.5cm}
\addtolength{\textwidth}{3cm}

\begin{document}

%\title{Review and some generalizations of the Mendelson's model}
\title{The ADHC user manual}

\author{
Bart{\l}omiej Jacek Kubica\\
\\
Institute of Information Technology\\
Warsaw University of Life Sciences -- SGGW\\
Nowoursynowska 159, 02-776~Warsaw, POLAND\\
\\
e-mail: \url{bartlomiej_kubica@sggw.edu.pl}, \url{bartlomiej.jacek.kubica@gmail.com}
}

\date{\today}

\maketitle

%\begin{abstract}

\section{Introduction}
\label{sec:intro}

ADHC (which stands for Algorithmic Differentiation and Hull-Consistency enforcing) is a template C++ library.

At the moment, the manual is pretty basic, but should be sufficient to get an idea on how to using the library; especiall in conjunction with the examples.
The manual is going to be extended in future versions of the library.


\section{Installation}
\label{sec:installation}

As a header-only library, ADHC does not require compilation.
All headers can be inlcuded by a simple:
%
\begin{verbatim}
#include <adhc.hpp>
\end{verbatim}
%
Nevertheless, ADHC is using the survive-CXSC library, for interval oparations.
This means that:
%
\begin{itemize}
\item the executable of the program using ADHC has to link \verb+-lcxsc+,
\item the environment variable \verb+XSC_DIR+ (in the Makefile of ADHC) has to be set to the proper path (unless the headers of survive-CXSC, and the lib itself are already included/linked properly, using other environment variables.
\end{itemize}


\section{Examples and tests}

Three targets are compiled by default, in the Makefile:
%
\begin{itemize}
\item \verb+examples+ -- a few examples of using various ADHC features,
\item \verb+tests+ -- unit tests based on the Boost.Test library,
\item \verb+gtests+ -- unit tests based on Google Test library.
\end{itemize}
%
If one does not want to use any of the unit test mechanisms, they can turn off making the related file, by changing the \verb+TARGETS+ variable in the Makefile.


\section{Syntax}

The basic type defined in ADHC is the template class representing an expression:
%
\begin{verbatim}
template<int level,
         sparsity_t sparse_mode,
         int num_vars,
         typename T = cxsc::interval>
struct adhc_ari {
    // ...
};
\end{verbatim}
%
The meaning of the template parameters is as follows:
%
\begin{itemize}
\item \verb+level+ -- information on what should be computed; number of computed derivatives (for nonnegative values) or construction of the syntactic tree (for value -1),
\item \verb+sparse_mode+ -- should sparse or dense matrix and vector representations be used,
\item \verb+num_vars+ -- the number of variables,
\item \verb+T+ -- the basic type of represented `numbers': \verb+cxsc::interval+ is the default one, but other types (\verb+cxsc::real+, \verb+cxsc::l_interval+, etc) can be used as well.
\end{itemize}
%
A function can be represented, e.g., in the following manner:

\begin{verbatim}
const int lev = 2; // we compute the function value,
                   // the gradient and the Hesse matrix
const int n = 3;   // the number of variables

adhc_ari<lev, sparse_mode, n, T> 
f(const adhc_vector<lev, sparse_mode, n, T> &x) {
    adhc_ari<lev, sparse_mode, n, T> result;
    result = sqr(x[1]) - sin(x[2]) + x[3] + 1.0;
    return result;
}
\end{verbatim}

Basing on such a representation, ADHC can compute/bound the function value, gradient, and Hesse matrix over a given interval (or at a given oint).
It can also generate a function to narrow the interval, by enforcing the hull-consistency (HC).

Functions for HC enforcing can be found in the file \verb+hull_cons.hpp+.


\section{Code snippets from ADHC 2.0}
\label{sec:code-snippets}

The current version of ADHC allows us to generate data over various datatypes: not only \verb+cxsc::interval+, but also many other types: pointwise and interval-valued, real-valued and complex, double-precision or staggered, etc.

For each of these types, different representations of function values, gradients, and Hesse matrices have to be used.
To maintain all of them, some type trait classes have been defined.
The most important of them, defined in file \verb+type_traits.hpp+, is simply called \verb+Types+.
It is parameterized by the underlying basic type.

Let us present its example instantiations:

\begin{verbatim}
template<>
struct Types<cxsc::interval> {
    using scalar_type = cxsc::interval;

    using dense_vector_type = cxsc::ivector;
    using dense_matrix_type = cxsc::imatrix;
    using dense_tensor_type = itensor;

    using sparse_vector_type = cxsc::sivector;
    using sparse_matrix_type = cxsc::simatrix;
    using sparse_tensor_type = sitensor;

    static const bool has_sparse_types = true;
};


template<>
struct Types<cxsc::cinterval> {
    using scalar_type = cxsc::cinterval;

    using dense_vector_type = cxsc::civector;
    using dense_matrix_type = cxsc::cimatrix;
    using dense_tensor_type = citensor;

    using sparse_vector_type = cxsc::scivector;
    using sparse_matrix_type = cxsc::scimatrix;
    using sparse_tensor_type = scitensor;

    static const bool has_sparse_types = true;
};
\end{verbatim}

Such instantiations exist also for the types: \verb+cxsc::l_interval+, \verb+cxsc::real+, \verb+cxsc::complex+, \verb+cxsc::l_real+.
More types may be added in the future.

Please note, that for the staggered-precision types, sparse representations of vectors and matrices do not exist in the current version of C-XSC (or survive-CXSC).

The above type trait classes declare also some tensor types to represent higher-order derivatives.
Such types have not been implemented yet, and it is questionable if they are worth the effort, as higher-order derivatives would probably be computed with severe overestimation.

In addition to the above type trait classes, type lists have been defined, allowing to easily check, which kind of types is under consideration:

\begin{verbatim}
using interval_types = TL::TypeList<cxsc::interval, cxsc::cinterval,
                                    cxsc::l_interval>;

using scalar_types = TL::TypeList<cxsc::real, cxsc::complex, cxsc::l_real,
                                  cxsc::interval, cxsc::cinterval,
                                  cxsc::l_interval>;
\end{verbatim}

There are also lists \verb+vector_types+ and \verb+matrix_types+.

Also, trait classes \verb+underlying_point_type+ and \verb+underlying_element_type+ are defined in the same file.
The former gets as the template parameter an interval type, and returns the related pointwise type.
The latter gets the vector or matrix type, and gives the type of a single element.

Using these classes, we can, for instance, create generic templated functions to compute the product of various types (file \verb+tensor_product.hpp+): scalar and vector, scalar and matrix, two vectors, vector and matrix, etc.

Let us present the version for two vectors of arbitrary elements:
%
\begin{verbatim}
template<class T>
typename std::enable_if<TL::IndexOf<adhc::vector_types, T>::value != -1,
         typename adhc::Types<typename underlying_element_type<T>::type>::
     dense_matrix_type>::type
tensor_product (const T &x, const T &y) {
    const int n = VecLen(x);
    const int m = VecLen(y);
    typename adhc::Types<typename underlying_element_type<T>::type>::
        dense_matrix_type result(n, m);
    for (int i = 1; i <= n; ++i) {
        result[i] = x[i]*y;
    }
    return result;
}

\end{verbatim}

The \verb+enable_if+ template is probably one of the least readable constructs of the C++ template meta-programming.
It has two template parameters: a condition and a type.
The template contains an inner type definition \verb+type+ if and only if, the condition is met: then \verb+type+ is equal to the second template parameter.
%Details can be found, i.a., in \cite{gennaro-book}.

The above examples have been given to illustrate the effort of implementing the generic library, compatible with several types.
%The whole code can be found on GitLab \cite{adhc}.

\end{document}

