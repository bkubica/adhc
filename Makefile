CXX = g++

#XSC_DIR = $(HOME)/cxsc-2_5_4
XSC_DIR = $(HOME)/survive-cxsc

TARGETS = examples tests gtests

HEADERS = adhc.hpp adhc_ari.hpp adhc_node.hpp adhc_defs.hpp adhc_vector.hpp max.hpp tensor_product.hpp product.hpp factory.hpp adhc_deriv_type.hpp deriv_of_product.hpp type_traits.hpp hull_cons.hpp


.PHONY: all clean


all: ${TARGETS} doc


examples: examples.cpp ${HEADERS}
	$(CXX) -o $@ $@.cpp -I$(XSC_DIR)/include/ -lcxsc -lm -L$(XSC_DIR)/lib/

tests: tests.cpp ${HEADERS}
	$(CXX) -o $@ $@.cpp -I$(XSC_DIR)/include/ -lcxsc -lm -lboost_unit_test_framework -lboost_test_exec_monitor -L$(XSC_DIR)/lib/

gtests: gtests.cpp ${HEADERS}
	$(CXX) -pthread -o $@ $@.cpp -I$(XSC_DIR)/include/ -lcxsc -lm -lgtest -lgtest_main -L$(XSC_DIR)/lib/


doc: doc/manual.pdf

doc/manual.pdf: doc/manual.tex
	cd doc; pdflatex manual.tex; pdflatex manual.tex; rm *.aux *.log; cd ..


clean:
	rm ${TARGETS}

