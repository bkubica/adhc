#if !defined __ADHC_DEFS_HPP__
#define __ADHC_DEFS_HPP__

enum class NodeType { undefined, constant, variable, function, int_power, operation};


enum class FunOperType { add, sub, mult, div,
	 		 sqr, sqrt, exp, ln,
		   	 neg,
		   	 sin, cos, tan, cot,
		   	 asin, acos, atan, acot,
		   	 sinh, cosh, tanh, coth,
		   	 max, abs,
		   	 intersect
};

#endif

