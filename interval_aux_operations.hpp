#if !defined __INTERVAL_AUX_OPERATIONS_HPP__
#define __INTERVAL_AUX_OPERATIONS_HPP__

namespace adhc {

inline cxsc::interval positive_base_power(const cxsc::interval &base, const cxsc::interval &exponent) {
	// For some peculiar reason, the function cxsc::pow(interval &, interval)
	// from file imath.cpp is *not* MT-safe!
	// Hence, a replacment is needed and here it goes.
	// Important!!! We assume that base is positive (as the name of function says...).
	return cxsc::exp(cxsc::ln(base) * exponent);
}


inline cxsc::interval root (const cxsc::interval &x, const int n) {
	if (Inf(x) > 0.0) return positive_base_power(x, cxsc::interval(1.0)/n);
	return 0.0 | positive_base_power(cxsc::interval(Sup(x)), cxsc::interval(1.0)/n);
}


static cxsc::real const q_ex2a = 6243314768165359.0 / 8796093022208.0; // a FiLib++ cnstant


inline cxsc::interval log_natural (const cxsc::interval &x) {
	//cout << "log_natural(" << x << ")\n";
	/*cxsc::interval ub{cxsc::Infinity};
	if (Sup(x) < cxsc::Infinity) ub = cxsc::ln(cxsc::interval(Sup(x)));
	if (Inf(x) > 0.0) return cxsc::ln(cxsc::interval(Inf(x))) | ub;
	else return -Infinity | ub;*/
	//----if (Inf(x) > 0.0) return cxsc::ln(x);
	//----return -Infinity | cxsc::ln(cxsc::interval(Sup(x)));
	if (Inf(x) > 0.0) {
		if (Sup(x) == cxsc::Infinity) return cxsc::ln(cxsc::interval(Inf(x))) | Infinity;
		else return cxsc::ln(x);
	}
	//cout << "aaa" << endl;
	if (Sup(x) == cxsc::Infinity) return cxsc::interval(-Infinity, 1e3);//Infinity);
	else return -Infinity | cxsc::ln(cxsc::interval(Sup(x)));
}


inline cxsc::interval exponens(const cxsc::interval &x) {
	if (Sup(x) < q_ex2a) return cxsc::exp(x);
	return cxsc::exp(cxsc::interval(Inf(x))) | Infinity;
}

} //end namespace adhc

#endif

