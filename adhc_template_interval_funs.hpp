template<class T>
inline typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1, T>::type power(const T &x, int n) {
	return cxsc::power(x, n);
}

// The enable_if is used mostly so that the function was not called for adhc_node!
// 
// For cxsc::interval, we have an optimized function Power(); hence the specialization.

template<>
inline cxsc::interval power<cxsc::interval>(const cxsc::interval &x, int n) {
	return cxsc::Power(x, n);
}

