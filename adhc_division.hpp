
#if !defined __ADHC_DIVISION_HPP__
#define __ADHC_DIVISION_HPP__

inline void safe_division_with_bounds (const cxsc::interval &dividend, const cxsc::interval &divisor,
		   const cxsc::interval &result_bounds, cxsc::interval &result) {
	if (0.0 <= dividend) {
		// cxsc: in C-XSC, for intervals, `` <= '' means ``belongs to''
		// also, we assume that divisor contains zero...
		result = result_bounds;
	}
	xinterval extended = dividend % divisor;
	cxsc::ivector vec = result_bounds/*cxsc::interval(-Infinity, Infinity)*/ & extended;
	if (vec[1] != EmptyIntval()) {
		if (vec[2] == EmptyIntval()) result = vec[1];
		else result = vec[1] | vec[2];
	}
	else {
		// no solutions???
		std::cout << "empty division of " << dividend << " and " << divisor << "\n";
		result = EmptyIntval();
	}
}


inline void safe_division (const cxsc::interval &dividend, const cxsc::interval &divisor, cxsc::interval &result) {
	safe_division_with_bounds (dividend, divisor, cxsc::interval(-Infinity, Infinity), result);
	/*xinterval extended = dividend % divisor;
	cxsc::ivector vec = cxsc::interval(-Infinity, Infinity) & extended;
	if (vec[1] != EmptyIntval()) {
		if (vec[2] == EmptyIntval()) result = vec[1];
		else result = vec[1] | vec[2];
	}
	else {
		// no solutions???
		std::cout << "empty division of " << dividend << " and " << divisor << "\n";
		result = EmptyIntval();
	}*/
}

template<class T>
inline typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1 && TL::IndexOf<interval_types, T>::value != -1, void>::type
safe_division (const T &dividend, const T &divisor, T &result) {
	result = dividend/divisor;
	//std::cout << "safe_division\n";
	//std::cout << cxsc::interval(-cxsc::Infinity, cxsc::Infinity) << std::endl;
	//safe_division_with_bounds (dividend, divisor, cxsc::interval(-cxsc::Infinity, cxsc::Infinity), result);
}

template<class T>
inline typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1 && TL::IndexOf<interval_types, T>::value == -1, void>::type
safe_division (const T &dividend, const T &divisor, T &result) {
	result = dividend/divisor;
}

template<class T>
typename std::enable_if<TL::IndexOf<vector_types, T>::value != -1, void>::type
safe_division (const T &dividend, const typename underlying_element_type<T>::type &divisor, T &result) {
	for (int i = 1; i <= VecLen(dividend); ++i) safe_division (dividend[i], divisor, result[i]);
}

void safe_division (const cxsc::sivector &dividend, const cxsc::interval &divisor, cxsc::sivector &result) {
	result = dividend;
	for (int ind = 0; ind < dividend.get_nnz(); ++ind) {
		safe_division (dividend.values()[ind], divisor, result.values()[ind]);
	}
}

template<class T>
typename std::enable_if<TL::IndexOf<matrix_types, T>::value != -1, void>::type
safe_division (const T &dividend, const typename underlying_element_type<T>::type &divisor, T &result) {
	for (int i = 1; i <= ColLen(dividend); ++i) {
		for (int j = 1; j <= RowLen(dividend); ++j) {
			safe_division (dividend[i][j], divisor, result[i][j]);
		}
	}
}

void safe_division (const cxsc::simatrix &dividend, const cxsc::interval &divisor, cxsc::simatrix &result) {
	result = dividend;
	for (int ind = 0; ind < dividend.get_nnz(); ++ind) {
		safe_division (dividend.values()[ind], divisor, result.values()[ind]);
	}
}


/*template<class T>
inline typename std::enable_if<TL::IndexOf<scalar_types, T>::value != -1 && TL::IndexOf<interval_types, T>::value != -1, void>::type
safe_division_with_bounds (const T &dividend, const T &divisor, const cxsc::interval &result_bounds, T &result) {
	std::cout << "a\n";
	if (0.0 <= dividend) {
		std::cout << "zero in dividend" << std::endl;
		// cxsc: in C-XSC, for intervals, `` <= '' means ``belongs to''
		// also, we assume that divisor contains zero...
		result = result_bounds;
	}
	if (0.0 <= divisor) result = result_bounds;
	else {
		std::cout << "result_bounds = " << result_bounds << std::endl;
		std::cout << "dividend = " << dividend << std::endl;
		std::cout << "divisor = " << divisor << std::endl;
		result = (dividend/divisor) & result_bounds;
	}
}*/

#endif

