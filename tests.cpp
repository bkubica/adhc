#include <iostream>
#include <exception>
#include <stdexcept>
#include <string>
#include <sstream>

#define BOOST_TEST_MODULE TestOfADHC
#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

#include <ivector.hpp>
#include <civector.hpp>
#include <cimatrix.hpp>
#include <scivector.hpp>
#include <scimatrix.hpp>
#include <l_ivector.hpp>
#include <l_imatrix.hpp>

#include "adhc.hpp"


using namespace std;
using namespace adhc;


#define SPARSITY SparsityLevel::highly_sparse

/*struct ZlyMianownik : public exception {
	const char *what() const throw() {
		return "Zerowy mianownik ulamka!\n";
	};
};*/

template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f_add(const adhc_vector<lev, sparse_mode, n> &x) {
	return x[1] + x[2];
}

BOOST_AUTO_TEST_CASE (test_addition) {
	cxsc::ivector x(2);
	const int num_vars = 2;
	x[1] = cxsc::interval(1.0, 2.0);
	x[2] = cxsc::interval(2.0, 4.0);
	adhc_vector<0, SPARSITY, num_vars> x_ (x);
	adhc_ari<0, SPARSITY, num_vars> y_ = f_add(x_);
	cxsc::interval y = std::get<0>(y_.deriv_value);
	cxsc::interval y_ref {3.0, 6.0};
	BOOST_CHECK (y == y_ref);
}


//****************************************************


template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> f_sub(const adhc_vector<lev, sparse_mode, n> &x) {
	return x[1] - x[2];
}

BOOST_AUTO_TEST_CASE (test_subtraction) {
	cxsc::ivector x(2);
	const int num_vars = 2;
	x[1] = cxsc::interval(1.0, 2.0);
	x[2] = cxsc::interval(2.0, 4.0);
	adhc_vector<1, SPARSITY, num_vars> x_ (x);
	adhc_ari<1, SPARSITY, num_vars> y_ = f_sub(x_);
	//std::cout << y_;

	cxsc::interval y = std::get<0>(y_.deriv_value);
	cxsc::ivector grad_y = std::get<1>(y_.deriv_value);

	cxsc::interval y_ref {-3.0, 0.0};
	cxsc::ivector grad_y_ref(2);
	grad_y_ref[1] = cxsc::interval(1.0);
	grad_y_ref[2] = cxsc::interval(-1.0);

	BOOST_CHECK_EQUAL (y, y_ref);
	BOOST_CHECK_EQUAL (grad_y, grad_y_ref);
}


//****************************************************


template<int lev, SparsityLevel sparse_mode, int n, typename T = cxsc::interval>
adhc_ari<lev, sparse_mode, n, T> f_comp_add(const adhc_vector<lev, sparse_mode, n, T> &x) {
	auto result = adhc_ari<lev, sparse_mode, n, T>::get_instance();
	result = T(1.0);
	for (int i = 1; i <= n; ++i) result += x[i];
	return result - sqr(x[1]);
}

//BOOST_PARAM_TEST_CASE (test_composite_addition, level)
BOOST_AUTO_TEST_CASE (test_composite_addition) {
	const int num_vars = 4;
	cxsc::ivector x(num_vars);
	for (int i = 1; i <= num_vars; ++i) x[i] = cxsc::interval(1.0, 2.0);
	adhc_vector<1, SPARSITY, num_vars> x_ (x);
	adhc_ari<1, SPARSITY, num_vars> y_ = f_comp_add(x_);
	//std::cout << y_;

	cxsc::interval y = std::get<0>(y_.deriv_value);
	cxsc::ivector grad_y = std::get<1>(y_.deriv_value);

	cxsc::interval y_ref {1.0, 8.0};
	cxsc::ivector grad_y_ref(num_vars);
	grad_y_ref = cxsc::interval(1.0);
	grad_y_ref[1] = cxsc::interval(1.0 - 2*x[1]);

	BOOST_CHECK_EQUAL (y, y_ref);
	BOOST_CHECK_EQUAL (grad_y, grad_y_ref);

	adhc_vector<-1, SPARSITY, num_vars> xx_ {x};
	//adhc_ari<-1, SPARSITY, num_vars> *yy_ptr = new adhc_ari<-1, SPARSITY, num_vars>;
	//adhc_ari<-1, SPARSITY, num_vars> &yy_ = *yy_ptr;
	adhc_ari<-1, SPARSITY, num_vars> yy_;
	yy_ = f_comp_add(xx_);
	std::cout << yy_ << "\n";
}


//****************************************************


using test_types = boost::mpl::list<cxsc::interval, cxsc::cinterval, cxsc::l_interval, cxsc::real, cxsc::complex, cxsc::l_real>;

BOOST_AUTO_TEST_CASE_TEMPLATE (basic_types_test, T, test_types) {
	std::cout << sizeof(T) << "\n";
	BOOST_CHECK((std::is_same<T, typename adhc::Types<T>::scalar_type>::value));
	const int n = 3;
	//adhc_ari<1, SparsityLevel::dense/*highly_sparse*/, n, T> xx_;
	//adhc_ari<1, SparsityLevel::sparse, n, T> xx_;
	adhc_ari<2, SparsityLevel::highly_sparse, n, T> xx_;
	xx_ = T(7.0);
	std::cout << xx_ << "\n";
}

BOOST_AUTO_TEST_CASE (test_default_template_value) {
	const int n = 3;
	const int level = 1;
	adhc_ari<level, SparsityLevel::highly_sparse, n> x_;
	x_ = cxsc::interval(4.0);
	std::cout << x_;
	BOOST_CHECK_EQUAL (std::get<0>(x_.deriv_value), cxsc::interval(4.0));
	adhc_ari<level, SparsityLevel::highly_sparse, n> y_;
	y_ = x_ * x_;
	std::cout << y_ << "\n";
	BOOST_CHECK_EQUAL (std::get<0>(y_.deriv_value), cxsc::interval(16.0));
}


//****************************************************


BOOST_AUTO_TEST_CASE (test_composite_addition_real) {
	const int num_vars = 4;
	cxsc::rvector x(num_vars);
	for (int i = 1; i <= num_vars; ++i) x[i] = cxsc::real(i);
	adhc_vector<1, SPARSITY, num_vars, cxsc::real> x_ (x);
	adhc_ari<1, SPARSITY, num_vars, cxsc::real> y_ = f_comp_add<1, SPARSITY, num_vars, cxsc::real>(x_);
	//std::cout << y_;

	cxsc::real y = std::get<0>(y_.deriv_value);
	cxsc::rvector grad_y = std::get<1>(y_.deriv_value);

	cxsc::real y_ref {10.0};
	cxsc::rvector grad_y_ref(num_vars);
	grad_y_ref = cxsc::real(1.0);
	grad_y_ref[1] = cxsc::real(1.0 - 2*x[1]);

	BOOST_CHECK_EQUAL (y, y_ref);
	BOOST_CHECK_EQUAL (grad_y, grad_y_ref);

	adhc_vector<-1, SPARSITY, num_vars> xx_ {cxsc::ivector(x)};
	//adhc_ari<-1, SPARSITY, num_vars> *yy_ptr = new adhc_ari<-1, SPARSITY, num_vars>;
	//adhc_ari<-1, SPARSITY, num_vars> &yy_ = *yy_ptr;
	adhc_ari<-1, SPARSITY, num_vars> yy_;
	yy_ = std::move(f_comp_add(xx_));
	std::cout << yy_ << "\n";
}


//****************************************************


BOOST_AUTO_TEST_CASE (test_underlying_point_type) {
	using some_type = underlying_point_type<cxsc::interval>::type;
	std::cout << typeid(some_type).name() << std::endl;
	some_type x;
	x = 44.77;
	std::cout << "x = " << x << "\n";
	bool val = std::is_same<some_type, cxsc::real>::value;
	BOOST_CHECK_EQUAL (val, true);

	bool val2 = std::is_same<underlying_point_type<cxsc::l_interval>::type, cxsc::l_real>::value;
	BOOST_CHECK (val2);

	underlying_point_type<cxsc::ivector>::type xx(2);
	xx[1] = 1; xx[2] = 2;
	std::cout << "xx = " << xx << "\n";
	bool val3 = std::is_same<underlying_point_type<cxsc::ivector>::type, cxsc::rvector>::value;
	BOOST_CHECK (val3);
}


//****************************************************


BOOST_AUTO_TEST_CASE (test_intersection) {
	std::cout << "===intersection===\n";
	const int num_vars = 2;
	cxsc::ivector x(num_vars);
	x[1] = cxsc::interval(-1.0, 2.0);
	x[2] = cxsc::interval(-5.0, -1.0);
	const int level = 2;
	adhc_vector<level, SparsityLevel::highly_sparse, num_vars> x_(x);
	std::cout << "std::cout << x[1]: ";
	std::cout << x_[1];
	std::cout << "std::cout << x[2]: ";
	std::cout << x_[2];
	adhc_ari<level, SparsityLevel::highly_sparse, num_vars> y_;
	y_ = intersection(x_[1] + (x_[1] + 1.0)* x_[2],  x_[1] * (1.0 + x_[2]) + x_[2]);
	std::cout << "std::cout << y: ";
	std::cout << y_;
	BOOST_CHECK_EQUAL (std::get<0>(y_.deriv_value), cxsc::interval(-13.0, 2.0));

	adhc_vector<-1, SparsityLevel::highly_sparse, num_vars> xx_(x);
	adhc_ari<-1, SparsityLevel::highly_sparse, num_vars>  yy_/*;
	yy_ =*/{ intersection(xx_[1] + (xx_[1] + 1.0)* xx_[2],  xx_[1] * (1.0 + xx_[2]) + xx_[2])};
	std::cout << yy_ << "\n";
}


//****************************************************



BOOST_AUTO_TEST_CASE (test_max_zero_interval) {
	std::cout << "===max===\n";
	const int num_vars = 3;
	cxsc::ivector x(num_vars);
	x = cxsc::interval(-2.0, 2.0);
	const int level = 2;
	adhc_vector<level, SparsityLevel::highly_sparse, num_vars> x_(x);
	std::cout << "std::cout << x[1]: ";
	std::cout << x_[1];
	std::cout << "std::cout << x[2]: ";
	std::cout << x_[2];
	adhc_ari<level, SparsityLevel::highly_sparse, num_vars> y_;
	y_ = max(x_[1], 0.0) + max(-x_[2], 1.0) + max(x_[1], x_[3] + 0.0);
	std::cout << "std::cout << y: ";
	std::cout << y_;
	BOOST_CHECK_EQUAL (std::get<0>(y_.deriv_value), cxsc::interval(-1.0, 6.0));

	adhc_vector<-1, SparsityLevel::highly_sparse, num_vars> xx_(x);
	adhc_ari<-1, SparsityLevel::highly_sparse, num_vars>  yy_/*;
	yy_ =*/{ max(xx_[1], 0.0) + max(-xx_[2], 1.0) + max(xx_[1], xx_[3] + 0.0)};
	std::cout << yy_;
	std::cout << "\n";
}

bool approximately_equal(const cxsc::l_interval &x, const cxsc::l_interval &y, const cxsc::real &eps) {
	return abs(Inf(x) - Inf(y)) < eps && abs(Sup(x) - Sup(y)) < eps;
}

BOOST_AUTO_TEST_CASE (test_max_zero_l_interval, * boost::unit_test::tolerance(1e-6)) {
	std::cout << "===max===\n";
	const int num_vars = 3;
	cxsc::l_ivector x(num_vars);
	x = cxsc::l_interval(-2.0, 2.0);
	const int level = 1;
	adhc_vector<level, SparsityLevel::highly_sparse, num_vars, cxsc::l_interval> x_(x);
	std::cout << x_[1];
	adhc_ari<level, SparsityLevel::highly_sparse, num_vars, cxsc::l_interval> y_;
	y_ = max(x_[1], 0.0) + max(x_[2], 1.0) + max(x_[1], x_[3]);
	std::cout << y_;
	BOOST_CHECK(approximately_equal(std::get<0>(y_.deriv_value), cxsc::l_interval(-1.0, 6.0), 1e-6));

	cxsc::ivector xi(num_vars);
	for (int i = 1; i <= VecLen(xi); ++i) xi[i] = cxsc::interval(x[i]);
	//xi = cxsc::ivector(x);
        //xi = cxsc::interval(-2.0, 2.0);
	adhc_vector<-1, SparsityLevel::highly_sparse, num_vars, cxsc::interval> xx_(xi);
	adhc_ari<-1, SparsityLevel::highly_sparse, num_vars, cxsc::l_interval>  yy_/*;
	yy_ =*/{ max(xx_[1], 0.0) + max(xx_[2], 1.0) + max(xx_[1], xx_[3] + 0.0)};
	std::cout << yy_ << "\n";
}

/*BOOST_AUTO_TEST_CASE (test_max_zero_real) {
	std::cout << "===max===\n";
	const int num_vars = 3;
	cxsc::rvector x(num_vars);
	x = 2.0;
	const int level = 1;
	adhc_vector<level, SparsityLevel::highly_sparse, num_vars, cxsc::real> x_(x);
	std::cout << x_[1];
	adhc_ari<level, SparsityLevel::highly_sparse, num_vars, cxsc::real> y_;
	y_ = max(x_[1], 0.0) + max(x_[2], 1.0) + max(x_[1], x_[3]);
	std::cout << y_;
	BOOST_CHECK_EQUAL (std::get<0>(y_.deriv_value), cxsc::l_interval(-1.0, 6.0));

	cxsc::ivector xi(num_vars);
	//for (int i = 1; i <= VecLen(xi); ++i) xi[i] = cxsc::interval(x[i]);
	xi = cxsc::ivector(x);
        //xi = cxsc::interval(-2.0, 2.0);
	adhc_vector<-1, SparsityLevel::highly_sparse, num_vars, cxsc::interval> xx_(xi);
	adhc_ari<-1, SparsityLevel::highly_sparse, num_vars, cxsc::l_interval>  yy_;
	yy_ = max(xx_[1], 0.0) + max(xx_[2], 1.0) + max(xx_[1], xx_[3] + 0.0);
	std::cout << yy_ << "\n";
}*/


/*BOOST_AUTO_TEST_CASE (test_nierownosci_ulamkow)
{
	Ulamek a{1, 2};
	Ulamek b{2, 3};
	BOOST_CHECK (a == b);
}

BOOST_AUTO_TEST_CASE (test_dodawania_ulamkow)
{
	Ulamek a{1, 2};
	Ulamek b{1, 3};
	Ulamek c{5, 6};
	BOOST_CHECK_EQUAL ((a + b), c);
}*/

