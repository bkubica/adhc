#if !defined __HULL_CONS_HPP__
#define __HULL_CONS_HPP__

#include "adhc_ari.hpp"
#include "interval_aux_operations.hpp"


namespace adhc {


void evaluate_nodes (adhc_node *node, const cxsc::ivector &variable_value) {
	switch (node->node_type) {
		case NodeType::undefined: std::cout << "Evaluating an undefined node! It will be a bug!\n"; break;
		case NodeType::constant: break;// already evaluated
		case NodeType::variable: node->value = variable_value[node->var_num]; break;
		case NodeType::function:
			evaluate_nodes (node->function_params.arg, variable_value);
			switch (node->function_params.fun) {
				case FunOperType::sqr: node->value = cxsc::sqr(node->function_params.arg->value); break;
				case FunOperType::exp: node->value = adhc::exponens(node->function_params.arg->value); break;
				case FunOperType::ln:  node->value = adhc::log_natural(node->function_params.arg->value); break;
				case FunOperType::neg: node->value = -node->function_params.arg->value; break;
				case FunOperType::sin: node->value = cxsc::sin(node->function_params.arg->value); break;
				case FunOperType::cos: node->value = cxsc::cos(node->function_params.arg->value); break;
				default: node->value = 0.0; std::cout << "Evaluating unknown function!\n";
			}; break;
		case NodeType::int_power:
			evaluate_nodes (node->int_power_params.arg, variable_value);
			//node->value = cxsc::power(node->int_power_params.arg->value, node->int_power_params.exponent);
			node->value = cxsc::Power(node->int_power_params.arg->value, node->int_power_params.exponent);
			break;
		case NodeType::operation:
			evaluate_nodes (node->operation_params.left, variable_value);
			evaluate_nodes (node->operation_params.right, variable_value);
			switch (node->operation_params.oper) {
				case FunOperType::add: node->value =
					       node->operation_params.left->value + node->operation_params.right->value;
					       break;
				case FunOperType::sub: node->value =
					       node->operation_params.left->value - node->operation_params.right->value;
					       break;
				case FunOperType::mult: node->value =
					       node->operation_params.left->value * node->operation_params.right->value;
					       break;
				case FunOperType::div: node->value =
					       node->operation_params.left->value / node->operation_params.right->value;
					       break;
				case FunOperType::max: node->value = cxsc::interval(
					       cxsc::max(Inf(node->operation_params.left->value),
							 Inf(node->operation_params.right->value)),
					       cxsc::max(Sup(node->operation_params.left->value),
							 Sup(node->operation_params.right->value))
					       );
					       break;
				case FunOperType::intersect: node->value =
					       node->operation_params.left->value & node->operation_params.right->value;
					       break;
				default: std::cout << "Evaluating unknown operation!\n";
			}; break;
		//default: node->value = 0.0; std::cout << "Evaluating unknown node type!\n";
	}
}


int hc_forward_evaluate (adhc_node *node, const cxsc::ivector &variable_value) {
	/*cout << "enorcing " << variable_value << "\n";;
	cout << "on node type: " << static_cast<int>(node->node_type) << "\n";
	cout << "inital value: " << node->value << "\n";
	if (node->node_type == NodeType::function) {
		if (node->function_params.fun == FunOperType::ln) cout << "ln\n";
		else cout << "function " << static_cast<int>(node->function_params.fun) << "\n";
	}
	if (node->node_type == NodeType::variable) cout << "x[" << node->var_num << "] = " << variable_value[node->var_num] << endl;*/
	cxsc::interval new_value;
	//if (node->node_type == NodeType::operation) cout << "operation " << static_cast<int>(node->operation_params.oper) << "\n";
	switch (node->node_type) {
		case NodeType::undefined: std::cout << "Evaluating an undefined node! It will be a bug!\n"; break;
		case NodeType::constant: return 0;// already evaluated
		case NodeType::variable: new_value = variable_value[node->var_num]; break;
		case NodeType::function:
			hc_forward_evaluate (node->function_params.arg, variable_value);
			switch (node->function_params.fun) {
				case FunOperType::sqr: new_value = cxsc::sqr(node->function_params.arg->value); break;
				case FunOperType::exp: new_value = adhc::exponens(node->function_params.arg->value); break;
				case FunOperType::ln:  new_value = adhc::log_natural(node->function_params.arg->value); break;
				case FunOperType::neg: new_value = -node->function_params.arg->value; break;
				case FunOperType::sin: new_value = cxsc::sin(node->function_params.arg->value); break;
				case FunOperType::cos: new_value = cxsc::cos(node->function_params.arg->value); break;
				default: new_value = 0.0;
					 std::cout << "Evaluating unknown function: ";
					 std::cout << static_cast<int>(node->function_params.fun) << "\n";
			};
			break;
		case NodeType::int_power:
			hc_forward_evaluate (node->int_power_params.arg, variable_value);
			//new_value = cxsc::power(node->int_power_params.arg->value, node->int_power_params.exponent);
			new_value = cxsc::Power(node->int_power_params.arg->value, node->int_power_params.exponent);
			break;
		case NodeType::operation:
			hc_forward_evaluate (node->operation_params.left, variable_value);
			hc_forward_evaluate (node->operation_params.right, variable_value);
			switch (node->operation_params.oper) {
				case FunOperType::add: new_value =
					       node->operation_params.left->value + node->operation_params.right->value;
					       break;
				case FunOperType::sub: new_value =
					       node->operation_params.left->value - node->operation_params.right->value;
					       break;
				case FunOperType::mult: new_value =
					       node->operation_params.left->value * node->operation_params.right->value;
					       break;
				case FunOperType::div: new_value =
					       node->operation_params.left->value / node->operation_params.right->value;
					       break;
				case FunOperType::max: new_value = cxsc::interval(
					       cxsc::max(Inf(node->operation_params.left->value),
						  	 Inf(node->operation_params.right->value)),
					       cxsc::max(Sup(node->operation_params.left->value),
						 	 Sup(node->operation_params.right->value))
					       );
					       break;
				case FunOperType::intersect: new_value =
					       node->operation_params.left->value & node->operation_params.right->value;
					       break;
				default: std::cout << "Evaluating unknown operation!\n";
			};
			break;
		//default: node->value = 0.0; std::cout << "Evaluating unknown node type!\n"; return -1;
	}
	//cout << "\tnode->value = " << node->value << "\n";
	//cout << "\tnew_value = " << new_value << "\n";
	if (Disjoint(node->value, new_value)) return -1;
	else node->value &= new_value;
	//cout << "Node type: " << static_cast<int>(node->node_type) << "\n";
	//if (node->node_type == NodeType::operation) cout << "type: " << static_cast<int>(node->operation_params.oper) << endl;
	//cout << "\tfinal value: " << node->value << "\n\n";
	return 0;
}


void enforce_on_periodic_function (const cxsc::interval &value,
				   const cxsc::interval &argument,
				   const cxsc::interval &period,
				   std::vector< cxsc::interval > &results) {
	cxsc::interval tmp_val = value;
	while (cxsc::Inf(tmp_val) <= cxsc::Sup(argument)) {
		if (!Disjoint(tmp_val, argument)) {
			results.push_back(tmp_val & argument);
		}
		tmp_val += period;
	}
	tmp_val = value - period;
	while (cxsc::Sup(tmp_val) >= cxsc::Inf(argument)) {
		if (!Disjoint(tmp_val, argument)) {
			results.push_back(tmp_val & argument);
		}
		tmp_val -= period;
	}
}


int hc_enforce (adhc_node *node,
		cxsc::ivector &variable_value,
		cxsc::intvector &which,
		const cxsc::interval &enforced_value) {
	// returned values: -1 - impossible, domains disjoint, 0 - no changes, 1 - domain reduced
	//std::cout << "We enforce " << enforced_value << " on node: " << static_cast<int>(node->node_type) << ", value: " << node->value << "\n";
	//if (node->node_type == NodeType::function) cout << "fun: " << static_cast<int>(node->function_params.fun) << "\n";
	//if (node->node_type == NodeType::variable) cout << "variable: " << node->var_num << "\n";
	//if (node->node_type == NodeType::operation) cout << "oper: " << static_cast<int>(node->operation_params.oper) << "\n";
	if (cxsc::Disjoint (node->value, enforced_value)) return -1;
	if (node->value <= enforced_value) {
		// cxsc: in C-XSC, for intervals, `` <= '' means ``belongs to''
		return 0;
	}
	// now, we know that node->value is going to change
	cxsc::interval value_enforced_on_arg, value_enforced_on_left, value_enforced_on_right;
	cxsc::interval value1, value2;
	std::vector< cxsc::interval > values;
	switch (node->node_type) {
		case NodeType::undefined: std::cout << "HC on an undefined node! It will be a bug!\n"; return -1;
		case NodeType::constant: break;// change node->value??? how to re-establish it later???
		case NodeType::variable:
			//node->value &= enforced_value;
			if (cxsc::Disjoint (variable_value[node->var_num], enforced_value)) {
				// Is it posible at this point? But better to check...
				return -1;
			}
			variable_value[node->var_num] &= enforced_value;
			node->value = variable_value[node->var_num];
			which[node->var_num] = 0;
			return 1;
		case NodeType::function:
			node->value &= enforced_value;
			//value_enforced_on_arg = 0.0;
			switch (node->function_params.fun) {
			       case FunOperType::exp: if (cxsc::Sup(node->value) < 0.0) return -1;
						      //if (cxsc::Inf(node->value) < 0.0) cxsc::SetInf(node->value, 0.0);
						      value_enforced_on_arg = adhc::log_natural(node->value); break;
			       case FunOperType::ln:  //if (cxsc::Sup(node->value) < 0.0) return -1;
						      //if (cxsc::Inf(node->value) < 0.0) cxsc::SetInf(node->value, 0.0);
						      value_enforced_on_arg = adhc::exponens(node->value); break;
			       case FunOperType::sqr: if (cxsc::Sup(node->value) < 0.0) return -1;
						      if (cxsc::Inf(node->value) < 0.0) cxsc::SetInf(node->value, 0.0);
						      value1 = cxsc::sqrt(node->value);
						      if (!cxsc::Disjoint(value1, node->function_params.arg->value)) {
							value_enforced_on_arg = value1 & node->function_params.arg->value;
							value2 = -value1;
							if (!cxsc::Disjoint(value2, node->function_params.arg->value)) {
								value_enforced_on_arg |= (value2 & node->function_params.arg->value);
							}
						      }
						      else {
							value2 = -value1;
							if (!cxsc::Disjoint(value2, node->function_params.arg->value)) {
								value_enforced_on_arg = value2 & node->function_params.arg->value;
							}
						      }
						      break;
			       case FunOperType::sqrt: value_enforced_on_arg = cxsc::sqr(node->value); break;
			       case FunOperType::neg: value_enforced_on_arg = -node->value; break;
			       case FunOperType::sin: if (cxsc::Inf(node->value) <= -1.0 && cxsc::Sup(node->value) >= 1.0) {
						      return 0;
					      }
					      //return 0;
					      value1 = cxsc::asin(node->value);
					      values.clear();
					      enforce_on_periodic_function (value1, node->function_params.arg->value, 2.0*cxsc::Pi_interval, values);
					      enforce_on_periodic_function (cxsc::Pi_interval - value1, node->function_params.arg->value, 2.0*cxsc::Pi_interval, values);
					      if (values.empty()) return -1;
					      value_enforced_on_arg = values[0];
					      for (size_t i = 1; i < values.size(); ++i) value_enforced_on_arg |= values[i];
					      break;
			       case FunOperType::cos: if (cxsc::Inf(node->value) <= -1.0 && cxsc::Sup(node->value) >= 1.0) {
						      return 0;
					      }
					      //return 0;
					      value1 = cxsc::acos(node->value);
					      values.clear();
					      enforce_on_periodic_function (value1, node->function_params.arg->value, 2*cxsc::Pi_interval, values);
					      enforce_on_periodic_function (-value1, node->function_params.arg->value, 2*cxsc::Pi_interval, values);
					      if (values.empty()) return -1;
					      value_enforced_on_arg = values[0];
					      for (size_t i = 1; i < values.size(); ++i) value_enforced_on_arg |= values[i];
					      break;
			       // ??? much missing here...
				default: std:: cout << "Narrowing for this function not implemented (yet?)!\n"; return 0;
			       }
			       if (hc_enforce (node->function_params.arg,variable_value,which,value_enforced_on_arg) == -1) return -1;
			       else return 1;
		case NodeType::int_power:
			node->value &= enforced_value;
			//return 0;
			if (node->int_power_params.exponent % 2 == 0) {
				// even exponent
			        //cxsc::interval value_enforced_on_arg = cxsc::power(node->value, real(1.0/node->exponent));;
				if (cxsc::Inf(node->value) < 0.0) {
					if (cxsc::Sup(node->value) < 0.0) return -1;
					else cxsc::SetInf(node->value, 0.0);
				}
				//value1 = cxsc::pow(node->value, cxsc::interval(1.0)/node->int_power_params.exponent);
				value1 = adhc::root(node->value, node->int_power_params.exponent);
				if (!cxsc::Disjoint(value1, node->function_params.arg->value)) {
					value_enforced_on_arg = value1 & node->function_params.arg->value;
					value2 = -value1;
					if (!cxsc::Disjoint(value2, node->function_params.arg->value)) {
						value_enforced_on_arg |= (value2 & node->function_params.arg->value);
					}
				}
				else {
					value2 = -value1;
					if (!cxsc::Disjoint(value2, node->function_params.arg->value)) {
						value_enforced_on_arg = value2 & node->function_params.arg->value;
					}
				}
		        	if (hc_enforce(node->function_params.arg, variable_value, which, value_enforced_on_arg) == -1) {
					return -1;
				}
				else return 1;
			}
			else {
				//return 0;
				// odd exponent
				if (cxsc::Inf(node->value) < 0.0) {
					//return 0;
					if (cxsc::Sup(node->value) > 0.0) {
						//return 0;
						value_enforced_on_arg =
							adhc::root(cxsc::interval(Sup(node->value)),
								node->int_power_params.exponent) |
						       	-adhc::root(cxsc::interval(-Inf(node->value)),
								node->int_power_params.exponent);
						       	/*cxsc::pow(cxsc::interval(Sup(node->value)),
								  cxsc::interval(1.0)/node->int_power_params.exponent) | 
							-cxsc::pow(cxsc::interval(-Inf(node->value)),
								   cxsc::interval(1.0)/node->int_power_params.exponent);*/
						//return 0;
					}
					else value_enforced_on_arg =
					     -adhc::root(-node->value, node->int_power_params.exponent);
					     //-cxsc::pow(-node->value, cxsc::interval(1.0)/node->int_power_params.exponent);
				} else {
					//return 0;
					//if (cxsc::Inf(node->value) <= 0.01) return 0;
					//try {
					value_enforced_on_arg =
					     adhc::root(node->value, node->int_power_params.exponent);
					     //cxsc::pow(node->value, cxsc::interval(1.0)/node->int_power_params.exponent);
					/*}
					catch(...) {
						std::cout << node->value << "\n";
						return 0;
					}*/
				}
				if(hc_enforce(node->function_params.arg, variable_value, which, value_enforced_on_arg) == -1) {
					return -1;
				}
				else return 1;
			}
		case NodeType::operation:
			node->value &= enforced_value;
			//cxsc::interval value_enforced_on_left, value_enforced_on_right;
			switch (node->operation_params.oper) {
				case FunOperType::add:
					value_enforced_on_left = node->value - node->operation_params.right->value;
					value_enforced_on_right = node->value - node->operation_params.left->value;
					break;
				case FunOperType::sub:
					value_enforced_on_left = node->value + node->operation_params.right->value;
					value_enforced_on_right = node->operation_params.left->value - node->value;
					break;
				case FunOperType::mult:
					if (0.0 <= node->operation_params.right->value) //value_enforced_on_left =
						//cxsc::interval(-cxsc::Infinity, cxsc::Infinity);
						safe_division_with_bounds (node->value,
									   node->operation_params.right->value,
									   node->operation_params.left->value,
									   value_enforced_on_left);
					else value_enforced_on_left = node->value/node->operation_params.right->value;
					if (0.0 <= node->operation_params.left->value) //value_enforced_on_right = 
						//cxsc::interval(-cxsc::Infinity, cxsc::Infinity);
						safe_division_with_bounds (node->value,
									   node->operation_params.left->value,
									   node->operation_params.right->value,
									   value_enforced_on_right);
					else value_enforced_on_right = node->value/node->operation_params.left->value;
					break;
				case FunOperType::div:
					value_enforced_on_left = node->value * node->operation_params.right->value;
					//value_enforced_on_right = node->operation_params.left->value / node->value;
					safe_division_with_bounds (node->operation_params.left->value,
								   node->value,
								   node->operation_params.right->value,
								   value_enforced_on_right);
					// what if node->value contains zero??????????????
					break;
				case FunOperType::max:
					//std::cout << "Can't we do better...?\n";
					value_enforced_on_left = cxsc::interval(-cxsc::Infinity, cxsc::Infinity);//node->operation_params.left->value;
					value_enforced_on_right = cxsc::interval(-cxsc::Infinity, cxsc::Infinity);//node->operation_params.right->value;
					if(Sup(node->operation_params.right->value) < Inf(node->value)) {
						if(Sup(node->operation_params.left->value) < Inf(node->value)) {
							return -1;
						}
						value_enforced_on_left = node->value;
						break;
					}
					if(Sup(node->operation_params.left->value) < Inf(node->value)) {
						value_enforced_on_right &= node->value;
					}
					break;
				case FunOperType::intersect: value_enforced_on_left = value_enforced_on_right = node->value;
						    	     break;
				default: std::cout << "HC on an unknown operation!\n";
					 return -1;
			};
			if (hc_enforce (node->operation_params.left, variable_value, which, value_enforced_on_left) == -1) {
				return -1;
			}
			if (hc_enforce(node->operation_params.right, variable_value, which, value_enforced_on_right) == -1) {
				return -1;
			}
			// What if left or right has been narrowed? Shouldn't thare be another propagation?
			return 1;
		//default: std::cout << "HC on an unknown node type! What a peculiar mistake!\n";
		//	 return -1;
	}
	std::cout << "Should we really be there?\n";
	return 0;
}


void count_variable_occurrences (adhc_node *node, cxsc::intvector &counts) {
	switch (node->node_type) {
		case NodeType::undefined: std::cout << "Counting variables in an undefined node! It will be a bug!\n"; break;
		case NodeType::constant: break;
		case NodeType::variable: node->value = ++counts[node->var_num]; break;
		case NodeType::function: count_variable_occurrences (node->function_params.arg, counts); break;
		case NodeType::int_power: count_variable_occurrences (node->int_power_params.arg, counts); break;
		case NodeType::operation: count_variable_occurrences (node->operation_params.left, counts);
					  count_variable_occurrences (node->operation_params.right, counts);
					  break;
		//default: std::cout << "Unknown node type!\n";
	}
}


} //end of namespace adhc


#endif

