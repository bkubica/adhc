#if !defined __MAX_HPP__
#define __MAX_HPP__

namespace adhc {

class adhc_node;

cxsc::real max(const cxsc::real &a, const cxsc::real &b) {
	//std::cout << "a\n";
	return cxsc::max(a, b);
}

cxsc::l_real max(const cxsc::l_real &a, const cxsc::real &b) {
	//std::cout << "b\n";
	if (a >= b) return a;
	else return cxsc::l_real{b};
}

template<class T>
inline 
typename std::enable_if<!std::is_same<T, adhc_node>::value && !std::is_arithmetic<T>::value /*!std::is_same<T, cxsc::real>::value*/, T>::type
max(const T &a, const T &b) {
	//std::cout << "c\n";
	if (a >= b) return a;
	else return b;
}

/*template<class T>
inline 
//typename std::enable_if<!std::is_same<T, adhc_node>::value && !std::is_same<T, cxsc::real>::value, T>::type
typename std::enable_if<TL::IndexOf<TL::TypeList<cxsc::real, cxsc::l_real>, T>::value, T>::type
max(T a, const real r) {
	if (a >= r) return a;
	else return T(r);
}*/

} //end namespace adhc

#endif

