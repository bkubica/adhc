#if !defined __ADHC_DERIV_TYPE_HPP__
#define __ADHC_DERIV_TYPE_HPP__

template<int deriv_num, SparsityLevel sparse_mode, int num_vars, typename T>
struct adhc_deriv_type {
        using deriv_type = typename adhc_types<sparse_mode, num_vars, T>::higher_deriv_type;
};

template<SparsityLevel sparse_mode, int num_vars, typename T>
struct adhc_deriv_type<2, sparse_mode, num_vars, T> {
	using deriv_type = typename adhc_types<sparse_mode, num_vars, T>::Hesse_type;
};

template<SparsityLevel sparse_mode, int num_vars, typename T>
struct adhc_deriv_type<1, sparse_mode, num_vars, T> {
	using deriv_type = typename adhc_types<sparse_mode, num_vars, T>::grad_type;
};

template<SparsityLevel sparse_mode, int num_vars, typename T>
struct adhc_deriv_type<0, sparse_mode, num_vars, T> {
	using deriv_type = typename adhc_types<sparse_mode, num_vars, T>::scalar_type;
};

#endif

