#if !defined __ADHC_NODE_HPP__
#define __ADHC_NODE_HPP__


struct adhc_node {
        NodeType node_type;

	cxsc::interval value;

        union {
		// nothing -- for node_type == constant
		int var_num; // for node_type == variable
		struct {
			// for node_type == function
			FunOperType fun;
			adhc_node *arg;
		} function_params;
		struct {
			// for node_type == int_power
			int exponent;
			adhc_node *arg;
		} int_power_params;
                struct {
			// for node_type == operation
			FunOperType oper;
			adhc_node *left, *right; 
                } operation_params;
                //adhc_node args[2]; // for node_type == operation
		//(adhc_node *)args[2];
        };

	adhc_node() : node_type(NodeType::undefined) {}

	explicit adhc_node (const cxsc::interval &value_) : node_type(NodeType::constant), value(value_) {}

	explicit adhc_node (const cxsc::real &value_) : node_type(NodeType::constant), value(value_) {}

	adhc_node(const adhc_node &u) : node_type(u.node_type), value(u.value) {
		//std::cout << "copy constructor " << static_cast<int>(node_type) << "\n";
		//if (u.node_type == NodeType::variable) std::cout << "x[" << u.var_num << "] = " << u.value << "\n";
		switch(node_type) {
			case NodeType::variable: var_num = u.var_num;
						 break;
			case NodeType::function: function_params.arg = new adhc_node (*u.function_params.arg);
						 function_params.fun = u.function_params.fun;
						 break;
			case NodeType::int_power:
					int_power_params.arg = new adhc_node (* u.int_power_params.arg);
					int_power_params.exponent = u.int_power_params.exponent;
					break;
			case NodeType::operation:
					operation_params.left = new adhc_node (* u.operation_params.left);
					operation_params.right = new adhc_node (* u.operation_params.right);
					operation_params.oper = u.operation_params.oper;
					break;
			case NodeType::constant: return;
			case NodeType::undefined: return;
			//default: return; // this covers undefined and constant
		}
	}

	adhc_node & operator = (const adhc_node &u) {
	//adhc_ari(const adhc_ari<-1, sparse_mode, num_vars> &u) : node_type(u.node_type), value(u.value) {
		//std::cout << "operator = " << static_cast<int>(u.node_type) << "\n";
		//if (u.node_type == NodeType::variable) std::cout << "x[" << u.var_num << "] = " << u.value << "\n";
		if (this == &u) {
			std::cout << "WOW!!!\n\n\n";
			return *this;
		}
		if (this->node_type != NodeType::undefined) {
			std::cout << "Assigning to a defined adhc_node!!!\n\n\n\n";
		}
		node_type = u.node_type;
		value = u.value;
		switch(node_type) {
			case NodeType::variable: var_num = u.var_num;
						 break;
			case NodeType::function:
					function_params.arg = new adhc_node (* u.function_params.arg);
					function_params.fun = u.function_params.fun;
					break;
			case NodeType::int_power:
					int_power_params.arg = new adhc_node (* u.int_power_params.arg);
					int_power_params.exponent = u.int_power_params.exponent;
					break;
			case NodeType::operation:
					operation_params.left = new adhc_node (* u.operation_params.left);
					operation_params.right = new adhc_node (* u.operation_params.right);
					operation_params.oper = u.operation_params.oper;
					break;
			case NodeType::constant: return *this;
			case NodeType::undefined: return *this;
			//default: return *this; // this covers undefined and constant
		}
		return *this;
	}

	adhc_node & shallow_copy() const {
		//std::cout << "shallow_copy " << static_cast<int>(node_type) << "\n";
		adhc_node *p_result = new adhc_node;
		p_result->node_type = node_type;
		p_result->value = value;
		switch(node_type) {
			case NodeType::variable: p_result->var_num = var_num;
						 break;
			case NodeType::function: p_result->function_params.arg = function_params.arg;
						 p_result->function_params.fun = function_params.fun;
						 break;
			case NodeType::int_power:
					p_result->int_power_params.arg = int_power_params.arg;
					p_result->int_power_params.exponent = int_power_params.exponent;
					break;
			case NodeType::operation:
					p_result->operation_params.left = operation_params.left;
					p_result->operation_params.right = operation_params.right;
					p_result->operation_params.oper = operation_params.oper;
					break;
			case NodeType::constant: return *p_result;;
			case NodeType::undefined: return *p_result;
			//default: return; // this covers undefined and constant
		}
		return *p_result;
	}

	~adhc_node() {
		//std::cout << "deleting " << static_cast<int>(node_type) << "\n";
		switch(node_type) {
			case NodeType::function:
				if (function_params.arg != nullptr && function_params.arg->node_type != NodeType::variable)
				//if (function_params.arg != nullptr)
					 delete function_params.arg;
				break;
			case NodeType::int_power:
				if (int_power_params.arg != nullptr && int_power_params.arg->node_type != NodeType::variable)
				//if (int_power_params.arg != nullptr)
					delete int_power_params.arg;
				break;
			case NodeType::operation:
				if (operation_params.left != nullptr && operation_params.left->node_type != NodeType::variable)
				//if (operation_params.left != nullptr)
					delete operation_params.left;
				if (operation_params.right != nullptr && operation_params.right->node_type != NodeType::variable)
				//if (operation_params.right != nullptr)
					delete operation_params.right;
			case (NodeType::variable, NodeType::constant, NodeType::undefined): return;
			//default: return; // this covers undefined, constant and variable
		}
	}

	std::string to_string() const {
		std::stringstream ss;
		switch(node_type) {
			case NodeType::undefined: ss << "(undefined)"; break;
			case NodeType::constant: ss << value; break;
			case NodeType::variable: ss << "x[" << var_num << "]"; break;
			case NodeType::function: //ss << "fun(" << static_cast<int>(function_params.fun) << ", ";
				switch (function_params.fun) {
					case FunOperType::sqr: ss << "sqr"; break;
					case FunOperType::sqrt: ss << "sqrt"; break;
					case FunOperType::exp: ss << "exp"; break;
					case FunOperType::ln: ss << "ln"; break;
					case FunOperType::neg: ss << "-"; break;
					case FunOperType::sin: ss << "sin"; break;
					case FunOperType::cos: ss << "cos"; break;
					//case FunOperType::exp: ss << "exp"; break;
					//case FunOperType::exp: ss << "exp"; break;
					default: ss << "other";
				}
				ss << "(" << function_params.arg->to_string() << ")"; break;
			case NodeType::int_power: ss << "power(" << int_power_params.exponent << ", ";
				ss << int_power_params.arg->to_string(); ss << ")"; break;
			case NodeType::operation:
				if (operation_params.oper == FunOperType::max) {
					ss << "max(";
					ss << operation_params.left->to_string();
					ss << ", ";
					ss << operation_params.right->to_string();
					ss << ")";
					break;
				}
				ss << "(" << operation_params.left->to_string();
				if (operation_params.oper == FunOperType::add) ss << " + ";
				else if (operation_params.oper == FunOperType::sub) ss << " - ";
				else if (operation_params.oper == FunOperType::mult) ss << " * ";
				else if (operation_params.oper == FunOperType::div) ss << " / ";
				else if (operation_params.oper == FunOperType::intersect) ss << " & ";
				//else if (operation_params.oper == FunOperType::neg) ss << "negation\n";
				//else if (operation_params.oper == FunOperType::sqr) ss << "sqr\n";
				else ss << "(other)";
				ss << operation_params.right->to_string() << ")";
				break;
			default: ss << "(unknown node value)";
		}
		return ss.str();
	}
};


//******************************************************************


std::ostream & operator << (ostream &s, const adhc_node &u) {
	s << u.to_string();
	return s;
}


//******************************************************************


adhc_node & operator + (const adhc_node &u, const adhc_node &v) {
	//std::cout << "operator +\n";
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::add;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	p_result->operation_params.right = const_cast<adhc_node *>(&v);

	return *p_result;
}

adhc_node & operator + (const adhc_node &u, const cxsc::interval &x) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::add;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	adhc_node *v_ptr = new adhc_node (x);
	p_result->operation_params.right = v_ptr;

	return *p_result;
}

adhc_node & operator + (const adhc_node &u, const cxsc::real &x) {
	adhc_node *p_result = new adhc_node;
	p_result->node_type = NodeType::operation;
	p_result->operation_params.oper = FunOperType::add;

	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	adhc_node *v_ptr = new adhc_node (x);
	p_result->operation_params.right = v_ptr;
	return *p_result;
}


//*************************************************************


adhc_node & operator += (adhc_node &u, const adhc_node &v) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());

	u.node_type = NodeType::operation;

	u.operation_params.oper = FunOperType::add;
	u.operation_params.left = p_u_old;
	u.operation_params.right = const_cast<adhc_node *>(&v);

	return u;
}

adhc_node & operator += (adhc_node &u, const cxsc::interval &x) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());
	u.node_type = NodeType::operation;
	u.operation_params.oper = FunOperType::add;
	u.operation_params.left = p_u_old;
	u.operation_params.right = new adhc_node (x);
	return u;
}

adhc_node & operator += (adhc_node &u, const cxsc::real &x) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());
	u.node_type = NodeType::operation;
	u.operation_params.oper = FunOperType::add;
	u.operation_params.left = p_u_old;
	u.operation_params.right = new adhc_node (x);
	return u;
}


//*************************************************************


adhc_node & operator - (const adhc_node &u, const adhc_node &v) {
	//std::cout << "operator -\n";
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::sub;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	p_result->operation_params.right = const_cast<adhc_node *>(&v);

	return *p_result;
}

adhc_node & operator - (const adhc_node &u, const cxsc::interval &x) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::sub;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	adhc_node *v_ptr = new adhc_node (x);
	p_result->operation_params.right = v_ptr;

	return *p_result;
}

adhc_node & operator - (const adhc_node &u, const cxsc::real &x) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::sub;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	adhc_node *v_ptr = new adhc_node (x);
	p_result->operation_params.right = v_ptr;

	return *p_result;
}


//*************************************************************


adhc_node & operator -= (adhc_node &u, const adhc_node &v) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());

	u.node_type = NodeType::operation;

	u.operation_params.oper = FunOperType::sub;
	u.operation_params.left = p_u_old;
	u.operation_params.right = const_cast<adhc_node *>(&v);

	return u;
}

adhc_node & operator -= (adhc_node &u, const cxsc::interval &x) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());
	u.node_type = NodeType::operation;
	u.operation_params.oper = FunOperType::sub;
	u.operation_params.left = p_u_old;
	u.operation_params.right = new adhc_node (x);
	return u;
}

adhc_node & operator -= (adhc_node &u, const cxsc::real &x) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());
	u.node_type = NodeType::operation;
	u.operation_params.oper = FunOperType::sub;
	u.operation_params.left = p_u_old;
	u.operation_params.right = new adhc_node (x);
	return u;
}


//*************************************************************


adhc_node & operator - (const adhc_node &u) {
	adhc_node *p_result = new adhc_node;
	p_result->node_type = NodeType::function;
	p_result->function_params.fun = FunOperType::neg;
	p_result->function_params.arg = const_cast<adhc_node *>(&u);
	return *p_result;
}


//*************************************************************


adhc_node & operator * (const adhc_node &u, const adhc_node &v) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::mult;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	p_result->operation_params.right = const_cast<adhc_node *>(&v);

	return *p_result;
}

adhc_node & operator * (const adhc_node &u, const cxsc::interval &x) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::mult;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	adhc_node *v_ptr = new adhc_node (x);
	p_result->operation_params.right = v_ptr;

	return *p_result;
}

adhc_node & operator * (const adhc_node &u, const cxsc::real &x) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::mult;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	adhc_node *v_ptr = new adhc_node (x);
	p_result->operation_params.right = v_ptr;

	return *p_result;
}


//*************************************************************


adhc_node & operator *= (adhc_node &u, const adhc_node &v) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());

	u.node_type = NodeType::operation;

	u.operation_params.oper = FunOperType::mult;
	u.operation_params.left = p_u_old;
	u.operation_params.right = const_cast<adhc_node *>(&v);

	return u;
}

adhc_node & operator *= (adhc_node &u, const cxsc::interval &x) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());
	u.node_type = NodeType::operation;
	u.operation_params.oper = FunOperType::mult;
	u.operation_params.left = p_u_old;
	u.operation_params.right = new adhc_node (x);
	return u;
}

adhc_node & operator *= (adhc_node &u, const cxsc::real &x) {
	//adhc_node *p_u_old = new adhc_node (u);
	adhc_node *p_u_old = &(u.shallow_copy());
	u.node_type = NodeType::operation;
	u.operation_params.oper = FunOperType::mult;
	u.operation_params.left = p_u_old;
	u.operation_params.right = new adhc_node (x);
	return u;
}


//*************************************************************


adhc_node & operator / (const adhc_node &u, const adhc_node &v) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;

	p_result->operation_params.oper = FunOperType::div;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	p_result->operation_params.right = const_cast<adhc_node *>(&v);

	return *p_result;
}


//*************************************************************

// symmetry of arithmetic operations of adhc_ari and a real or an interval

template <typename T>
inline
typename std::enable_if<std::is_arithmetic<T>::value || TL::IndexOf<adhc::scalar_types, T>::value != -1, adhc_node &>::type
operator + (const T &x, const adhc_node &u) {
	//std::cout << "zamiana kolejnosci (-1)\n";
	return *(new adhc_node (u + x));
}

template <typename T>
inline
typename std::enable_if<std::is_arithmetic<T>::value || TL::IndexOf<adhc::scalar_types, T>::value != -1, adhc_node & >::type
operator - (const T &x, const adhc_node &u) {
	return *(new adhc_node (-u + x));
}

template <typename T>
inline
typename std::enable_if<std::is_arithmetic<T>::value || TL::IndexOf<adhc::scalar_types, T>::value != -1, adhc_node &>::type
operator * (const T &x, const adhc_node &u) {
	return *(new adhc_node (u*x));
}


//*********************************************


adhc_node & sqr(const adhc_node &u) {
	//std::cout << "sqr\n";
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::function;
	p_result->function_params.fun = FunOperType::sqr;
	p_result->function_params.arg = const_cast<adhc_node *>(&u);
	//p_result->function_params.arg = new adhc_node (u);
	return *p_result;
}


adhc_node & power(const adhc_node &u, const int n) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::int_power;
	p_result->int_power_params.exponent = n;
	p_result->int_power_params.arg = const_cast<adhc_node *>(&u);
	//p_result->function_params.arg = new adhc_node (u);
	return *p_result;
}


adhc_node & exp(const adhc_node &u) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::function;
	p_result->function_params.fun = FunOperType::exp;
	p_result->function_params.arg = const_cast<adhc_node *>(&u);
	//p_result->function_params.arg = new adhc_node (u);
	return *p_result;
}


adhc_node & sin(const adhc_node &u) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::function;
	p_result->function_params.fun = FunOperType::sin;
	p_result->function_params.arg = const_cast<adhc_node *>(&u);
	//p_result->function_params.arg = new adhc_node (u);
	return *p_result;
}


adhc_node & cos(const adhc_node &u) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::function;
	p_result->function_params.fun = FunOperType::cos;
	p_result->function_params.arg = const_cast<adhc_node *>(&u);
	//p_result->function_params.arg = new adhc_node (u);
	return *p_result;
}


//*********************************************


adhc_node & max(const adhc_node &u, const adhc_node &v) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;
	p_result->function_params.fun = FunOperType::max;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	p_result->operation_params.right = const_cast<adhc_node *>(&v);
	return *p_result;
}


//*********************************************


adhc_node & max(const adhc_node &u, const cxsc::real &r) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;
	p_result->function_params.fun = FunOperType::max;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	p_result->operation_params.right = new adhc_node(r);
	return *p_result;
}


//*********************************************



//*********************************************


adhc_node & intersection (const adhc_node &u, const adhc_node &v) {
	adhc_node *p_result = new adhc_node;

	p_result->node_type = NodeType::operation;
	p_result->function_params.fun = FunOperType::intersect;
	p_result->operation_params.left = const_cast<adhc_node *>(&u);
	p_result->operation_params.right = const_cast<adhc_node *>(&v);
	return *p_result;
}

#endif

